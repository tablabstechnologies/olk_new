package com.pk.olk

import android.app.ProgressDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.pk.olk.adapter.GstStateAdapter
import com.pk.olk.databinding.ActivityAddCustomerBinding
import com.pk.olk.model.GenericModelClass
import com.pk.olk.model.StateGst
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Pattern
import com.pk.olk.utils.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddCustomerActivity : AppCompatActivity() {

    lateinit var binding:ActivityAddCustomerBinding
     var selectedTaxSatate: String? =null
     var selectedSatate:String?=null

    val listStateName= ArrayList<String>()
    val listStateCode= ArrayList<String>()
    var suppileCode:String="3"
    var listGst=ArrayList<StateGst>()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_add_customer)
        callState()

      binding.state.setAdapter(  ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listStateName))
      binding.taxState.setAdapter(  ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listStateName))

        binding.tabCustomerSupplier.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if(tab?.position==0){
                    binding.appBar.setText("Add Customer")
                    suppileCode="3"
                }else if(tab?.position==0){
                    binding.appBar.setText("Add Supplier")
                    suppileCode="19"
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })
        binding. tabLayout.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if(tab?.position==0){
                    binding. llDetails.visibility= View.GONE
                    binding.llBank.visibility= View.GONE
                    binding.llTax.visibility=View.VISIBLE


                }else if(tab?.position==1){
                    binding. llDetails.visibility= View.VISIBLE
                    binding.llBank.visibility= View.GONE
                    binding.llTax.visibility=View.GONE

                }else{
                    binding. llDetails.visibility= View.GONE
                    binding.llBank.visibility= View.VISIBLE
                    binding.llTax.visibility=View.GONE
                }

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })


       /* binding.switchCustomer.setOnCheckedChangeListener(object:CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if(isChecked){

                    binding.switchCustomer.setText("Customer  ")
                    binding.appBar.setText("Add Customer ")
                    suppileCode="3"
                }else{

                    binding.switchCustomer.setText("Supplier  ")
                    binding.appBar.setText("Add Supplier")
                    suppileCode="19"

                }

            }

        })*/


        binding.btnSave.setOnClickListener {

            checkCondition()
        }

        binding.addGST.setOnClickListener {
            if(!TextUtils.isEmpty(selectedTaxSatate)){
                if(!TextUtils.isEmpty(binding.gstin.text.toString())){

                    callSaveGST()

                }else{
                    CustomeToast.Show(this,"Please Select State")
                }

            }else{
                CustomeToast.Show(this,"Please Select State")
            }
        }

        binding.taxState.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                selectedTaxSatate = parent.getItemAtPosition(position).toString()
                // Display the clicked item using toast

            }

        binding.state.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                selectedSatate = parent.getItemAtPosition(position).toString()
                // Display the clicked item using toast

            }
    }

    private fun callSaveGST() {
        val temState=selectedTaxSatate

        val stateGst=StateGst()
        stateGst.Gst=binding.gstin.text.toString()
        stateGst.stateName=temState
        for (i in 0..listStateName.size-1){
            if(listStateName.get(i).equals(selectedSatate)){

                stateGst.stateCode=listStateCode.get(i)


            }
        }
        listGst.add(stateGst)
        binding.recViewGST.layoutManager=LinearLayoutManager(this)
        binding.recViewGST.adapter=GstStateAdapter(this,listGst,object :GstStateAdapter.AddState{
            override fun addedState() {
                TODO("Not yet implemented")
            }

        });

        }






    private fun checkCondition() {

        if(!TextUtils.isEmpty(binding.Name.text.toString())){
            if(!TextUtils.isEmpty(binding.Address.text.toString())){
                if(!TextUtils.isEmpty(binding.pinCode.text.toString())){

                    if(checkEmail()){
                    if(checkValidPAN()){
                        if(checkValidGST()){
                            callApi()
                        }
                    }
                    }



                }else{
                    CustomeToast.Show(this," Enter Pin Code")
                }

            }else{
                CustomeToast.Show(this," Enter Address")
            }
        }else{
            CustomeToast.Show(this," Enter Name")
        }



    }

    private fun callApi() {

        //6543


        val objects = JsonObject()
        val bankdetails = JsonObject()

        objects.addProperty("custname", binding.Name.text.toString())
        objects.addProperty("csflag", suppileCode)
        objects.addProperty("custaddr", binding.Address.text.toString())
        objects.addProperty("custstate", selectedSatate)
        objects.addProperty("pincode", binding.pinCode.text.toString())
        objects.addProperty("custphone", binding.contact.text.toString())
        objects.addProperty("custemail", binding.emailId.text.toString())
        objects.addProperty("custfax", " ")
        objects.addProperty("custpan", binding.panNumber.text.toString())
        objects.addProperty("custtan", " ")

        bankdetails.addProperty("accountno",binding.acNumber.text.toString())
        bankdetails.addProperty("bankname",binding.bankName.text.toString())
        bankdetails.addProperty("ifsc",binding.IFSC.text.toString())
        bankdetails.addProperty("branchname", binding.branchName.text.toString())
        objects.add("bankdetails",bankdetails)

        if (listGst.size!=0){
            val gstin:JsonObject=JsonObject();
            for (i in 0..listGst.size){

                gstin.addProperty(listGst.get(i).stateCode,listGst.get(i).Gst)
            }

            objects.add("gstin",gstin);

        }



        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val token: String = Utils.getCompanyData(this)!!
        val call = SaasCall.apiInterfacePaas.createCustomerSupplier(token, body)

        val progressDailog: ProgressDialog = ProgressDialog(this)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()
        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
                println("The Response is " + response.body()?.gkstatus)
                println("The Response is " + response.code())
                progressDailog.dismiss()
                if (response.body()?.gkstatus.equals("0")) {

                    CustomeToast.Show(this@AddCustomerActivity, "Data Added Sucessfuly ")
                } else {
                    CustomeToast.Show(this@AddCustomerActivity, "Failed to save data")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@AddCustomerActivity, "User Already Found")
                println("The Response is Fail;")
                println("The Response is Fail")
                progressDailog.dismiss()

            }

        })

    }


    private fun callState(){
        val states="{\"gkstatus\": 0, \"gkresult\": [{\"statecode\": 35, \"statename\": \"Andaman and Nicobar Islands\"}, {\"statecode\": 28, \"statename\": \"Andhra Pradesh\"}, {\"statecode\": 37, \"statename\": \"Andhra Pradesh (New)\"}, {\"statecode\": 12, \"statename\": \"Arunachal Pradesh\"}, {\"statecode\": 18, \"statename\": \"Assam\"}, {\"statecode\": 10, \"statename\": \"Bihar\"}, {\"statecode\": 4, \"statename\": \"Chandigarh\"}, {\"statecode\": 22, \"statename\": \"Chhattisgarh\"}, {\"statecode\": 26, \"statename\": \"Dadra and Nagar Haveli\"}, {\"statecode\": 25, \"statename\": \"Daman and Diu\"}, {\"statecode\": 7, \"statename\": \"Delhi\"}, {\"statecode\": 30, \"statename\": \"Goa\"}, {\"statecode\": 24, \"statename\": \"Gujarat\"}, {\"statecode\": 6, \"statename\": \"Haryana\"}, {\"statecode\": 2, \"statename\": \"Himachal Pradesh\"}, {\"statecode\": 1, \"statename\": \"Jammu and Kashmir\"}, {\"statecode\": 20, \"statename\": \"Jharkhand\"}, {\"statecode\": 29, \"statename\": \"Karnataka\"}, {\"statecode\": 32, \"statename\": \"Kerala\"}, {\"statecode\": 31, \"statename\": \"Lakshdweep\"}, {\"statecode\": 23, \"statename\": \"Madhya Pradesh\"}, {\"statecode\": 27, \"statename\": \"Maharashtra\"}, {\"statecode\": 14, \"statename\": \"Manipur\"}, {\"statecode\": 17, \"statename\": \"Meghalaya\"}, {\"statecode\": 15, \"statename\": \"Mizoram\"}, {\"statecode\": 13, \"statename\": \"Nagaland\"}, {\"statecode\": 21, \"statename\": \"Odisha\"}, {\"statecode\": 34, \"statename\": \"Pondicherry\"}, {\"statecode\": 3, \"statename\": \"Punjab\"}, {\"statecode\": 8, \"statename\": \"Rajasthan\"}, {\"statecode\": 11, \"statename\": \"Sikkim\"}, {\"statecode\": 33, \"statename\": \"Tamil Nadu\"}, {\"statecode\": 36, \"statename\": \"Telangana\"}, {\"statecode\": 16, \"statename\": \"Tripura\"}, {\"statecode\": 9, \"statename\": \"Uttar Pradesh\"}, {\"statecode\": 5, \"statename\": \"Uttarakhand\"}, {\"statecode\": 19, \"statename\": \"West Bengal\"}]}"
        val ob= JSONObject(states)
        val array=ob.getJSONArray("gkresult")
        for (i in 0..array.length()-1) {
          val code=  array.getJSONObject(i).getString("statecode")
          val name=  array.getJSONObject(i).getString("statename")
            listStateName.add(name)
            listStateCode.add(code)

        }
    }



    private fun checkEmail():Boolean{
        if(!TextUtils.isEmpty(binding.emailId.text.toString())){
            if(Pattern.regex_matcher(Pattern.EMAIL,binding.emailId.text.toString())){
                return true
            }else{
                CustomeToast.Show(this,"Please Enter Valid Email")
                return false
            }



        }else{
            return true
        }
    }

    private fun checkValidGST(): Boolean {
        if(!TextUtils.isEmpty(binding.gstin.text.toString())){
            if(Pattern.regex_matcher(Pattern.GSTIN,binding.gstin.text.toString())){
                return true
            }else{
                CustomeToast.Show(this,"Please Enter Valid GST Number")
                return false
            }



        }else{
            return true
        }
    }

    private fun checkValidPAN(): Boolean {
        if(!TextUtils.isEmpty(binding.panNumber.text.toString())){
            if(Pattern.regex_matcher(Pattern.panPatter,binding.panNumber.text.toString())){
                return true
            }else{
                CustomeToast.Show(this,"Please Enter Valid PAN Number")
                return false
            }
        }else{
            return true
        }
    }


}