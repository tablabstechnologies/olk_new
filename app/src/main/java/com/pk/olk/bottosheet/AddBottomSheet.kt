package com.pk.olk.bottosheet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pk.olk.*


class AddBottomSheet : BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view:View=inflater.inflate(R.layout.bottomsheet_add,null)
      /*  val textCust:TextView=view.findViewById(R.id.textCust)
        textCust.setOnClickListener {
            context?.startActivity(Intent(context,AddCustomerActivity::class.java))

        }*/

        /*val textProduct:TextView=view.findViewById(R.id.textProduct)
        textProduct.setOnClickListener {
            context?.startActivity(Intent(context,AddProductActivity::class.java))

        }*/

        val textSellIncoice:TextView=view.findViewById(R.id.textSellIncoice)
        textSellIncoice.setOnClickListener {
            context?.startActivity(Intent(context,GenerateInvoiceActivity::class.java))

        }
        val textPurchesInoice:TextView=view.findViewById(R.id.textPurchesInoice)
        textPurchesInoice.setOnClickListener {
            context?.startActivity(Intent(context,GenerateInvoiceActivity::class.java))

        }

        return view
    }
}