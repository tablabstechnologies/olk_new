    package com.pk.olk
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.JsonObject
import com.pk.olk.databinding.ActivityLogin2Binding
import com.pk.olk.model.GenericModelClass
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import com.pk.olk.viewmodel.LoginViewModel
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

    class LoginActivity : AppCompatActivity() {
    lateinit var context: Context

    lateinit var mainActivityViewModel: LoginViewModel
    lateinit var activityBinding: ActivityLogin2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityBinding = DataBindingUtil.setContentView(this, R.layout.activity_login2)

        context = this@LoginActivity

        mainActivityViewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        activityBinding.setLifecycleOwner(this)
        activityBinding.modelLogin = mainActivityViewModel

        activityBinding.email.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(editable: Editable?) {
                if (editable.isNullOrBlank()){
                    activityBinding.email.setBackgroundResource(R.drawable.border_textview_grey)

                }else{
                    activityBinding.email.setBackgroundResource(R.drawable.border_textview_lodin)

                }
            }
        })
        activityBinding.password.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(editable: Editable?) {
                if (editable.isNullOrBlank()){
                    activityBinding.password.setBackgroundResource(R.drawable.border_textview_grey)

                }else{
                    activityBinding.password.setBackgroundResource(R.drawable.border_textview_lodin)

                }
            }
        })



        activityBinding.btnLogIn.setOnClickListener {
            if (TextUtils.isEmpty(activityBinding.email.text.toString()) ||
                TextUtils.isEmpty(activityBinding.password.text.toString())
            ) {
                val toast: Toast =
                    Toast.makeText(this, "Please Enter Email and Password", Toast.LENGTH_LONG)
                toast.show()
                return@setOnClickListener

            }

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()

            mainActivityViewModel.getUser(
                activityBinding.password.text.toString(),
                activityBinding.email.text.toString()
            )!!.observe(this, Observer { serviceSetterGetter ->

                if (serviceSetterGetter.pwstatus.equals("0")) {
                    progressDailog.dismiss()
                    println("token"+serviceSetterGetter.token)
                    println("The Response Login success;")
                    Utils.setLoginResponse(serviceSetterGetter.token,this)

                    startActivity(Intent(this,CompanyListActivity::class.java))
                    finish()

                    Log.e("succ", mainActivityViewModel.toString());

                } else {
                    CustomeToast.Show(this,"Wrong email or password")
                    Log.e("DIG", mainActivityViewModel.toString());
                    println("User is not Found")
                    progressDailog.dismiss()
                }
            })
        }

        activityBinding.textForgotPassword.setOnClickListener {
            if (!TextUtils.isEmpty(activityBinding.email.text.toString())){
                forgotPassAPI()
            }else{
                CustomeToast.Show(this,"Please enter email address and then click forgot password")
            }
        }



       /* activityBinding.imageCall.setOnClickListener {

        }*/
        activityBinding.imageWhatApp.setOnClickListener {

            val url = "https://api.whatsapp.com/send?phone=$+919324596722"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)

        }

        activityBinding.imageYouTube.setOnClickListener {
            val url = "https://www.youtube.com/channel/UCjaKgb1BfZdQ99siiZKlGOg"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)

        }

        activityBinding.imageEmail.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.putExtra(Intent.EXTRA_EMAIL, "contact@onlinekhata.co.in")

            intent.type = "message/rfc822"
            startActivity(Intent.createChooser(intent, "Select email"))

        }
    }

    private fun forgotPassAPI() {
        val objects:JsonObject = JsonObject()
        objects.addProperty("accountemail",activityBinding.email.text.toString())

        val body:RequestBody = RequestBody.create(
                "application/json; charset=utf-8".toMediaTypeOrNull(),
                objects.toString()
        )

        val call =SaasCall.apiInterface.forgotpassword(body)

        val progressDialog: ProgressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        progressDialog.setMessage("Loading....")
        progressDialog.show()

        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                    call: Call<GenericModelClass>,
                    response: Response<GenericModelClass>){
                println("The Response is "+response.body()?.gkstatus)
                println("The Response is "+response.code())
                progressDialog.dismiss()

                if(response.body()?.gkstatus.equals("0")){
                    CustomeToast.Show(applicationContext,"Password reset email sent successfully!")

                }else{
                    CustomeToast.Show(this@LoginActivity,"Email not found")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@LoginActivity,"Email Not Found")
                println("The Response is Fail")
                progressDialog.dismiss()

            }

        })


    }


}