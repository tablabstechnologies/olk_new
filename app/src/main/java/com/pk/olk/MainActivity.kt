package com.pk.olk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.pk.olk.utils.Utils

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Handler().postDelayed({ // This method will be executed once the timer is over

            if(Utils.getLoginData(this)==null){


                val intent = Intent(this@MainActivity, LoginRegistrationActivity::class.java)
                startActivity(intent)
                finish()

            }else{

                if(Utils.getCompanyData(this)==null){
                    val intent =
                        Intent(this@MainActivity, CompanyListActivity::class.java)
                    startActivity(intent)
                    finish()
                }else {
                    val intent =
                        Intent(this@MainActivity, DashboardActivity::class.java)
                    startActivity(intent)
                    finish()
                }


            }



//            val intent =
//                Intent(this@MainActivity, PdfCreatorExampleActivity::class.java)
//            startActivity(intent)
//            finish()



        }, 2000)

    }
}