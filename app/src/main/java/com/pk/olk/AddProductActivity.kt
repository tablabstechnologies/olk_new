package com.pk.olk

import UnitData
import UnitList
import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.gson.JsonObject
import com.pk.olk.databinding.ActivityAddProductBinding
import com.pk.olk.model.GenericModelClass
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddProductActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener ,
    AdapterView.OnItemClickListener {
    lateinit var bindig: ActivityAddProductBinding
    lateinit var listUnitName: ArrayList<String>
    lateinit var listUnit: ArrayList<UnitData>
    var selectedItem: String?=null
    val NEW_SPINNER_ID = 1
    var listPopupWindow: ListPopupWindow? = null
    var products = arrayOf(
        "Camera", "Laptop", "Watch", "Smartphone",
        "Television"
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindig = DataBindingUtil.setContentView(this, R.layout.activity_add_product)

        bindig.btnSave.setOnClickListener {

            checkValidation()
        }
        listUnit = ArrayList()
        listUnitName = ArrayList()
        listUnitName.add("Unit")

        callApiUnit()
//        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listUnitName)
//        bindig.unitSearch.setAdapter(adapter)
//        bindig.unitSearch.setTextColor(Color.BLACK)
//            bindig.unitSearch.onItemClickListener(OnItemClickListener { adapterView, view, i, l ->
//            val value = adapterView.getItemAtPosition(i).toString()
//            Toast.makeText(this, value, Toast.LENGTH_SHORT).show()
//        })
//        bindig.unitSearch.onItemClickListener =
//            AdapterView.OnItemClickListener { parent, view, position, id ->
//                selectedItem = parent.getItemAtPosition(position).toString()
//                // Display the clicked item using toast
//
//            }


//
//        var aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, listUnitName)
//        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//
//        with(bindig.unitSearch)
//        {
//            adapter = aa
//            setSelection(0, false)
//            onItemSelectedListener = this@AddProductActivity
////            prompt = "Select your favourite language"
//            gravity = Gravity.CENTER
//
//        }
        listPopupWindow = ListPopupWindow(
            this
        )
        listPopupWindow!!.setAdapter(
            ArrayAdapter<Any?>(
                this,
                R.layout.list_layout_spinner,R.id.alert_filter_name, listUnitName as List<Any?>
            )
        )
        listPopupWindow!!.anchorView =  bindig.unitSearch
        listPopupWindow!!.width = 900
        listPopupWindow!!.height = 800
        listPopupWindow!!.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.res_black_menuroundfilled_corner));

//        val background =
//            ContextCompat.getDrawable(this, R.drawable.res_black_menuroundfilled_corner)
//        listPopupWindow!!.setBackgroundDrawable(background)
//        listPopupWindow!!.anchorView (R.drawable.border_textview_lodin)


        listPopupWindow!!.isModal = true
        listPopupWindow!!.setOnItemClickListener(
            this
        )
        bindig.unitSearch.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listPopupWindow!!.show()
            }
        })


    }
    override fun onItemClick(
        parent: AdapterView<*>?, view: View?,
        position: Int, id: Long
    ) {
        bindig.unitSearch.setText(listUnitName.get(position))
        listPopupWindow!!.dismiss()
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
        showToast(message = "Nothing selected")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        when (view?.id) {
            1 -> showToast(message = "Spinner 2 Position:${position} and language: ${listUnitName[position]}")
        }
    }

    private fun showToast(context: Context = applicationContext, message: String, duration: Int = Toast.LENGTH_LONG) {
        Toast.makeText(context, message, duration).show()
    }

    private fun callApiUnit() {
        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getAllUnit(token)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<UnitList> {
                override fun onResponse(
                    call: Call<UnitList>,
                    response: Response<UnitList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {
                        listUnit.addAll(response?.body()!!.gkresult)
                        for (i in 0..listUnit.size - 1) {
                            listUnitName.add(listUnit.get(i).unitname)
                        }

                        Log.i("FilterType", listUnitName.toString())

                    } else {
                        // CustomeToast.Show(this@AddProductActivity, "")

                    }

                }

                override fun onFailure(call: Call<UnitList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@AddProductActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }

    }

    private fun checkValidation() {
        if (!TextUtils.isEmpty(bindig.Name.text.toString())) {
            if (!TextUtils.isEmpty(selectedItem)) {

                callCreateProduct()



            } else {
                CustomeToast.Show(this, "Please Select Unit")
            }

        } else {
            CustomeToast.Show(this, "Please Enter Name")
        }
    }

    private fun callCreateProduct() {

        val objects: JsonObject = JsonObject()
        val productdetails: JsonObject = JsonObject()
        productdetails.addProperty("prodmrp", bindig.mrp.text.toString())
        productdetails.addProperty("productdesc", bindig.Name.text.toString())
        productdetails.addProperty("prodsp", bindig.salePrice.text.toString())
        productdetails.addProperty("amountdiscount",bindig.discountPrise.text.toString())
        productdetails.addProperty("percentdiscount",bindig.discountPersentage.text.toString())
        productdetails.addProperty("uomid", "84")
        productdetails.addProperty("openingstock", bindig.mrp.text.toString())
        productdetails.addProperty("gscode","123456")
        productdetails.addProperty("gsflag","7")
        objects.add("productdetails",productdetails)
        objects.addProperty("godownflag","False")

        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val token: String = Utils.getCompanyData(this)!!
        val call = SaasCall.apiInterfacePaas.createproducts(token, body)

        val progressDailog: ProgressDialog = ProgressDialog(this)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()
        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
                println("The Response is " + response.body()?.gkstatus)
                println("The Response is " + response.code())
                progressDailog.dismiss()
                if (response.body()?.gkstatus.equals("0")) {


                } else {
                    CustomeToast.Show(this@AddProductActivity, "OTP not Match")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@AddProductActivity, "User Already Found")
                println("The Response is Fail;")
                println("The Response is Fail")
                progressDailog.dismiss()

            }

        })


    }
}