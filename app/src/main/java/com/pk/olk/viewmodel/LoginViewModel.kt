package com.pk.olk.viewmodel

import android.text.TextUtils
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pk.olk.retrofit.ApiCallBack
import com.pk.olk.model.GenericModelClass

class LoginViewModel : ViewModel(){

/*    var servicesLiveData: MutableLiveData<GenericModelClass>? = null*/
    var servicesLiveData: MutableLiveData<GenericModelClass>?=null

    fun getUser(password:String,email:String) : LiveData<GenericModelClass>? {

        servicesLiveData = ApiCallBack.checkUserValid(password,email)
        return servicesLiveData
    }
}