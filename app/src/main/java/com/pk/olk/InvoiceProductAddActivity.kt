package com.pk.olk

import UnitList
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import com.google.gson.JsonObject
import com.pk.olk.databinding.ActivityInvoiceProductAddBinding
import com.pk.olk.model.ProductAdd
import com.pk.olk.model.ProductData
import com.pk.olk.model.ProductList
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InvoiceProductAddActivity : AppCompatActivity() {
    lateinit var binding:ActivityInvoiceProductAddBinding
    lateinit var listProduct:ArrayList<ProductData>
    lateinit var listProductName:ArrayList<String>
    lateinit var productAdd:ProductAdd
    lateinit var productData: ProductData



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_invoice_product_add)
        listProduct= ArrayList()
        listProductName= ArrayList()
        callApi()

        if(intent.hasExtra("data")){
            productAdd= intent.getSerializableExtra("data") as ProductAdd
            productData=productAdd.productData
            binding.searchProduct.setText(productData.productdesc)
            binding.rate.setText(productAdd.rate)
            binding.quntity.setText(productAdd.totalQuntity)
            binding.freeQuantity.setText(productAdd.freeQuntity)
            binding.discountAmount.setText(productAdd.discount)
            binding.gstPesentage.setText(productAdd.gstPesentage)
            checkalidationAuto()


        }

        binding.searchProduct.setAdapter( ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listProductName))

        binding.addProduct.setOnClickListener {
            checkalidation()
            val intent= Intent();
            intent.putExtra("data",productAdd)
            setResult(1,intent)
            finish()
        }

        binding.searchProduct.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                productData=listProduct.get(position)
                callTax(listProduct.get(position).productcode)

                // Display the clicked item using toast

            }

        binding.discountAmount.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                checkalidationAuto()
            }
        })

        binding.rate.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                checkalidationAuto()
            }
        })

        binding.quntity.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                checkalidationAuto()
            }
        })

        binding.freeQuantity.addTextChangedListener(object :TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                checkalidationAuto()
            }
        })





    }

    private fun callTax(productcode: Int) {

        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getTax(token,productcode)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<JsonObject> {
                override fun onResponse(
                    call: Call<JsonObject>,
                    response: Response<JsonObject>
                ) {
                    progressDailog.dismiss()

                    val objects:JSONObject= JSONObject(response.body().toString())
                   if(objects.getString("gkstatus").equals("0")){

                       binding.gstPesentage.setText(objects.getString("gkresult"))
                       checkalidationAuto()

                   }


                }

                override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@InvoiceProductAddActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }


    }

    private fun checkalidation() {
        if(!TextUtils.isEmpty(binding.rate.text.toString())){
            if(!TextUtils.isEmpty(binding.quntity.text.toString())){

                chekCalculation()




            }else{
                CustomeToast.Show(this,"Please Enter quantity")
            }


        }else{
            CustomeToast.Show(this,"Please Enter Rate")
        }

    }


    private fun checkalidationAuto() {
        if(!TextUtils.isEmpty(binding.rate.text.toString())){
            if(!TextUtils.isEmpty(binding.quntity.text.toString())){

                chekCalculation()






            }else{
               // CustomeToast.Show(this,"Please Enter quantity")
            }


        }else{
           // CustomeToast.Show(this,"Please Enter Rate")
        }

    }

    private fun chekCalculation() {
        var freeQuntity=0
        var discount=0
        if(!TextUtils.isEmpty(binding.freeQuantity.text.toString())){
            freeQuntity=binding.freeQuantity.text.toString().toInt()
        }
        if(!TextUtils.isEmpty(binding.discountAmount.text.toString())){
            discount=binding.discountAmount.text.toString().toInt()
        }

        var totCalclutedQntity=binding.quntity.text.toString().toInt()
        var totCalculatedAmount=binding.rate.text.toString().toDouble()*totCalclutedQntity
        var total:Double=totCalculatedAmount-discount

        val gst=binding.gstPesentage.text.toString()
        var totVal:Double=gst.toDouble()/100
        var totGst=total*totVal
        binding.taxbaleValue.setText("Taxbale Amount"+total)
        total=total+totGst
        binding.gstAmount.setText(""+totGst)

        if(productData==null){

        }else{
            productAdd= ProductAdd(total.toString(),freeQuntity.toString(),binding.quntity.text.toString(),discount.toString(),binding.rate.text.toString(),productData,totGst.toString(),binding.gstPesentage.text.toString())

        }

    }

    private fun callApi() {
        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getProducts(token)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<ProductList> {
                override fun onResponse(
                    call: Call<ProductList>,
                    response: Response<ProductList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {

                        listProduct.addAll(response.body()!!.gkresult)

                        for (i in 0..listProduct.size-1){

                            listProductName.add(listProduct.get(i).productdesc)
                        }


                    } else {

                    }

                }

                override fun onFailure(call: Call<ProductList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@InvoiceProductAddActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }
}