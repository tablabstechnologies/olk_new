package com.pk.olk

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateFormat
import android.text.format.Time
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import com.pk.olk.adapter.AddProductAdapter
import com.pk.olk.databinding.ActivityGenerateInvoiceBinding
import com.pk.olk.model.*
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import interfaces.EditItem
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList


class GenerateInvoiceActivity : AppCompatActivity() {

    lateinit var binding:ActivityGenerateInvoiceBinding
    var listProduct=ArrayList<ProductAdd>()
    var adapter: AddProductAdapter? =null
    var grandTotal:Double=0.0
    var taxAmount:Double=0.0
    var discountAmont:Double=0.0
    var total:Double=0.0
    var position:Int = 0
    var listCustomer=ArrayList<CustmerData>()
    var listCustomerName=ArrayList<String>()
    var customerDeatils: CustomerDetails? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_generate_invoice)
        binding.recViewProduct.layoutManager=LinearLayoutManager(this)

        adapter= AddProductAdapter(this,listProduct,object:EditItem{
            override fun editItemPrduct(pos: Int) {
                position=pos
                val intent=Intent(this@GenerateInvoiceActivity,InvoiceProductAddActivity::class.java)
                intent.putExtra("data",listProduct.get(pos))
                startActivityForResult(intent,12)
            }

            override fun deleteItem(pos: Int) {
                val productData: ProductAdd =listProduct.get(pos)

                //listProduct.add(productData)
                grandTotal=grandTotal-productData.totalAount.toDouble()
                discountAmont=discountAmont-productData.discount.toDouble()
                taxAmount=taxAmount-productData.gst.toDouble()

                binding.grandTotal.setText(""+grandTotal)
                binding.discountAmount.setText(""+discountAmont)
                binding.taxbaleAmount.setText(""+taxAmount)
                listProduct.removeAt(pos)
                adapter?.notifyDataSetChanged()
                if(listProduct.size==0){
                    binding.rlGrandTotal.visibility=View.GONE
                    binding.dateOfSupplay.visibility=View.GONE
                    binding.reveseCharge.visibility=View.GONE
                    binding.llmodeofTransportation.visibility=View.GONE
                    binding.radioGrop.visibility=View.GONE
                    binding.llpaymentDeatils.visibility=View.GONE
                    binding.save.visibility=View.GONE

                }
            }

        })
        binding.recViewProduct.adapter=adapter

        binding.AddProduct.setOnClickListener {

            val intent=Intent(this,InvoiceProductAddActivity::class.java)
            startActivityForResult(intent,11)

        }
        callgetCustomer()

        binding.save.setOnClickListener {
            /*saveinvoice()*/
            val intent=Intent(this,PdfCreatorExampleActivity::class.java)
            intent.putExtra("data",listProduct)
            intent.putExtra("tot",grandTotal)
            startActivity(intent)

        }

        binding.searchName.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->

                callgetSupplierDeatils(listCustomer.get(position).custid)

                // Display the clicked item using toast

            }

        binding.paymentDeatils.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val array=this@GenerateInvoiceActivity.resources.getStringArray(R.array.payment)
                binding.llBank.visibility=View.GONE
                binding.transcationID.visibility=View.GONE
                if(array[position].equals("Bank")){
                    binding.llBank.visibility=View.VISIBLE
                }
                if(array[position].equals("Online")){
                    binding.transcationID.visibility=View.VISIBLE

                }

            }

        }
        binding.moreDetails.setOnClickListener {
            binding.llmodeofTransportation.visibility=View.VISIBLE
            binding.radioGrop.visibility=View.VISIBLE
            binding.hideMoreDetails.visibility=View.VISIBLE
            binding.moreDetails.visibility=View.GONE
        }
        binding.hideMoreDetails.setOnClickListener {
            binding.llmodeofTransportation.visibility=View.GONE
            binding.radioGrop.visibility=View.GONE
            binding.hideMoreDetails.visibility=View.GONE
            binding.moreDetails.visibility=View.VISIBLE

        }

        binding.modeofTranscation.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val array=this@GenerateInvoiceActivity.resources.getStringArray(R.array.transportation)
                binding.vehicleNumber.visibility=View.GONE

                if(array[position].equals("Road")){
                    binding.vehicleNumber.visibility=View.VISIBLE
                }


            }

        }

        val sdf = SimpleDateFormat("dd-M-yyyy")
        val currentDate = sdf.format(Date())
        binding.date.setText(currentDate)
        binding.date.setOnClickListener {
            callRegiCalender(binding.date)

        }
        binding.dateOfSupplay.setOnClickListener {
            callRegiCalender(binding.dateOfSupplay)

        }


    }

    /*private fun saveinvoice() {


            val objects: JsonObject = JsonObject()
            val productdetails: JsonObject = JsonObject()
            productdetails.addProperty("prodmrp", bindig.mrp.text.toString())
            productdetails.addProperty("productdesc", bindig.Name.text.toString())
            productdetails.addProperty("prodsp", bindig.salePrice.text.toString())
            productdetails.addProperty("amountdiscount",bindig.discountPrise.text.toString())
            productdetails.addProperty("percentdiscount",bindig.discountPersentage.text.toString())
            productdetails.addProperty("uomid", "84")
            productdetails.addProperty("openingstock", bindig.mrp.text.toString())
            productdetails.addProperty("gscode","123456")
            productdetails.addProperty("gsflag","7")
            objects.add("productdetails",productdetails)
            objects.addProperty("godownflag","False")

            val body: RequestBody = RequestBody.create(
                "application/json; charset=utf-8".toMediaTypeOrNull(),
                objects.toString()
            )

            val token: String = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.createproducts(token, body)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()
            call.enqueue(object : Callback<GenericModelClass> {
                override fun onResponse(
                    call: Call<GenericModelClass>,
                    response: Response<GenericModelClass>
                ) {
                    println("The Response is " + response.body()?.gkstatus)
                    println("The Response is " + response.code())
                    progressDailog.dismiss()
                    if (response.body()?.gkstatus.equals("0")) {


                    } else {
                        CustomeToast.Show(this@AddProductActivity, "OTP not Match")

                    }
                }

                override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@AddProductActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })




    }*/

    private fun callRegiCalender(textView: TextView) {
        val calendar: Calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        val dateDlg = DatePickerDialog(
            this,
            { view, year, monthOfYear, dayOfMonth ->
                var strDate: CharSequence? = null
                val chosenDate = Time()
                chosenDate.set(dayOfMonth, monthOfYear, year)
                val dtDob: Long = chosenDate.toMillis(true)
                strDate = DateFormat.format("dd-MM-yyyy", dtDob)
                textView.setText(strDate)

            }, year, month, day
        )

        dateDlg.show()
    }


    private fun callgetCustomer() {

        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getCustomer(token)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<CustomerList> {
                override fun onResponse(
                    call: Call<CustomerList>,
                    response: Response<CustomerList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {



                        listCustomer.addAll(response.body()!!.gkresult)
                        callgetSupplier()


                    } else {

                    }

                }

                override fun onFailure(call: Call<CustomerList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@GenerateInvoiceActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }

    private fun callgetSupplier() {

        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getSupplier(token)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<CustomerList> {
                override fun onResponse(
                    call: Call<CustomerList>,
                    response: Response<CustomerList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {


                        println("In Suppliers")
                        listCustomer.addAll(response.body()!!.gkresult)
                        //
                        for (i in 0..listCustomer.size-1){
                            listCustomerName.add(listCustomer.get(i).custname)
                        }
                        binding.searchName.setAdapter(ArrayAdapter<String>(this@GenerateInvoiceActivity,android.R.layout.simple_list_item_1,listCustomerName))





                    } else {

                    }

                }

                override fun onFailure(call: Call<CustomerList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@GenerateInvoiceActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }


    private fun callgetSupplierDeatils( id: Int) {

        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getSupplierDeatils(token,id)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<CustomerDetails> {
                override fun onResponse(
                    call: Call<CustomerDetails>,
                    response: Response<CustomerDetails>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {

                        customerDeatils= response.body()!!





                    } else {

                    }

                }

                override fun onFailure(call: Call<CustomerDetails>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@GenerateInvoiceActivity, "User Already Found")
                    println("The Response is Fail;")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode==11){
            if(resultCode==1) {
                val productData: ProductAdd = data?.getSerializableExtra("data") as ProductAdd
                listProduct.add(productData)
                grandTotal=grandTotal+productData.totalAount.toDouble()
                discountAmont=discountAmont+productData.discount.toDouble()
                taxAmount=taxAmount+productData.gst.toDouble()
                binding.grandTotal.setText(""+grandTotal)
                binding.discountAmount.setText(""+discountAmont)
                binding.taxbaleAmount.setText(""+taxAmount)
                adapter?.notifyDataSetChanged()
                binding.rlGrandTotal.visibility=View.VISIBLE
                binding.dateOfSupplay.visibility=View.VISIBLE
                binding.reveseCharge.visibility=View.VISIBLE
                binding.llpaymentDeatils.visibility=View.VISIBLE
                binding.save.visibility=View.VISIBLE
                binding.moreDetails.visibility=View.VISIBLE
            }
        }

        if(requestCode==12){
            if(resultCode==1) {
                val productDataOne: ProductAdd = data?.getSerializableExtra("data") as ProductAdd
                val productData: ProductAdd =listProduct.get(position)

                //listProduct.add(productData)
                grandTotal=grandTotal-productData.totalAount.toDouble()
                discountAmont=discountAmont-productData.discount.toDouble()
                taxAmount=taxAmount-productData.gst.toDouble()

                grandTotal=grandTotal+productDataOne.totalAount.toDouble()
                discountAmont=discountAmont+productDataOne.discount.toDouble()
                taxAmount=taxAmount+productDataOne.gst.toDouble()
                binding.grandTotal.setText(""+grandTotal)
                binding.discountAmount.setText(""+discountAmont)
                binding.taxbaleAmount.setText(""+taxAmount)

                listProduct.removeAt(position)
                listProduct.add(productDataOne)

                adapter?.notifyDataSetChanged()


            }
        }

    }
    fun createJson (){
        val tempdt1 =   binding.date.text.toString();
        val tempdt2 =   binding.dateOfSupplay.text.toString();
        val df = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter.ofPattern("dd-MM-yyyy")
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        val d1 = LocalDate.parse(tempdt1, df)
        val dtofsupply = LocalDate.parse(tempdt2, df)


        val mainOb=JsonObject();
        val invoice=JsonObject();
        val consignee=JsonObject();
        val content=JsonObject();
        val productcode=JsonObject();
        val freeqty=JsonObject();
        val tax=JsonObject();
        val cess=JsonObject();
        val discount=JsonObject();
        val pricedetails=JsonObject();
        val av=JsonObject();
        val product=JsonObject();
        val prodData= JsonObject();
        val invnarration =JsonObject();
        val bankdetails=JsonObject();
        val stock=JsonObject();
        val items=JsonObject();

        invoice.addProperty("invoiceno","1234567")
        invoice.addProperty("ewaybillno","")
        invoice.addProperty("invoicedate","")
        invoice.addProperty("inoutflag","15")
        invoice.addProperty("taxstate","")
        invoice.addProperty("sourcestate","")
        invoice.addProperty("address","")
        invoice.addProperty("pincode","")
        invoice.addProperty("custid","")
        invoice.addProperty("issuername","")
        invoice.addProperty("designation","1")//confirm this
        invoice.addProperty("roundoffflag","0")
        invoice.addProperty("discflag","1")
        invoice.addProperty("taxflag","7")
        invoice.addProperty("transportationmode","None")
        invoice.addProperty("vehicleno","")
        invoice.addProperty("orgstategstin","")
        invoice.addProperty("reversecharge","")
        invoice.addProperty("paymentmode","3")
        invoice.addProperty("invoicetotal","500.00")
        invoice.addProperty("invtotalword","One Thousand One Hundred and Twenty")

        consignee.addProperty("consigneename","")
        consignee.addProperty("tinconsignee","")
        consignee.addProperty("gstinconsignee","")
        consignee.addProperty("gstinconsignee","")
        consignee.addProperty("consigneestate","")
        consignee.addProperty("consigneestatecode","")
        consignee.addProperty("consigneepincode","")

        productcode.addProperty("rate","")
        productcode.addProperty("quantity","")
        content.add("productcode",productcode)// check this

        invoice.add("content",content)//check this

        freeqty.addProperty("productcode","")
        freeqty.addProperty("quantity","")

        invoice.add("freevqty",freeqty)//check this

        tax.addProperty("productcode","")
        invoice.add("tax",tax)//check this

        cess.addProperty("productcode","")
        invoice.add("cess",cess)//check this

        discount.addProperty("productcode","")
        invoice.add("discount",discount)

        pricedetails.addProperty("custid","")
        pricedetails.addProperty("productcode","")
        pricedetails.addProperty("inoutflag","")
        pricedetails.addProperty("lastprice","")

        product.addProperty("name of product","")//check
        product.addProperty("Taxable Value","")//check

        prodData.addProperty("code of product","")//check
        prodData.addProperty("Taxable Value","")//check
        av.add("product",product)
        av.add("prodData",prodData)

        invnarration.addProperty("Narration","")
        invnarration.addProperty("Date of supply","")//check

        bankdetails.addProperty("accountno","")
        bankdetails.addProperty("bankname","")
        bankdetails.addProperty("ifsc","")
        bankdetails.addProperty("branchname","")

        stock.addProperty("inout","")

        items.addProperty("productcode","")
        items.addProperty("quantity","")










        mainOb.add("invoice",invoice)




    }
}