package com.pk.olk.model

import com.google.gson.annotations.SerializedName

data class CustmerData(@SerializedName("custid") var custid : Int,
                       @SerializedName("custname") var custname : String)
