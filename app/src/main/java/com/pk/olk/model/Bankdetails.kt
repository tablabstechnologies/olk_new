package com.pk.olk.model

import com.google.gson.annotations.SerializedName

data class Bankdetails(@SerializedName("ifsc") var ifsc : String,
                       @SerializedName("bankname") var bankname : String,
                       @SerializedName("accountno") var accountno : String,
                       @SerializedName("branchname") var branchname : String)
