package com.pk.olk.model

import com.google.gson.annotations.SerializedName

data class CustomerList(@SerializedName("gkstatus") var gkstatus : Int,
                        @SerializedName("gkresult") var gkresult : List<CustmerData>)
