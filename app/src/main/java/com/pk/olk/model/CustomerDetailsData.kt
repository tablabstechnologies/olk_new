package com.pk.olk.model

import com.google.gson.annotations.SerializedName

data class CustomerDetailsData(@SerializedName("custid") var custid : Int,
                               @SerializedName("custname") var custname : String,
                               @SerializedName("custaddr") var custaddr : String,
                               @SerializedName("custphone") var custphone : String,
                               @SerializedName("custemail") var custemail : String,
                               @SerializedName("custfax") var custfax : String,
                               @SerializedName("custpan") var custpan : String,
                               @SerializedName("custtan") var custtan : String,
                               @SerializedName("state") var state : String,
                               @SerializedName("custdoc") var custdoc : String,
                               @SerializedName("csflag") var csflag : Int,
                               @SerializedName("gstin") var gstin : String,
                               @SerializedName("pincode") var pincode : String,
                               @SerializedName("bankdetails") var bankdetails : Bankdetails)
