package com.pk.olk.model

import com.google.gson.annotations.SerializedName

data class ProductList ( @SerializedName("gkstatus") var gkstatus : Int,
                         @SerializedName("gkresult") var gkresult : List<ProductData>
)

