package com.pk.olk.model

import com.google.gson.annotations.SerializedName

data class CustomerDetails(@SerializedName("gkstatus") var gkstatus : Int,
                           @SerializedName("gkresult") var gkresult : CustomerDetailsData)
