package com.pk.olk.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data  class ProductData ( @SerializedName("srno") var srno : Int,
                          @SerializedName("unitname") var unitname : String,
                          @SerializedName("categoryname") var categoryname : String,
                          @SerializedName("productcode") var productcode : Int,
                          @SerializedName("productdesc") var productdesc : String,
                          @SerializedName("categorycode") var categorycode : String,
                          @SerializedName("productquantity") var productquantity : String,
                          @SerializedName("gsflag") var gsflag : Int): Serializable



