package com.pk.olk.model

import java.io.Serializable

data class ProductAdd(val totalAount:String,val freeQuntity:String,
                      val totalQuntity:String,val discount:String,
                      val rate:String,val productData: ProductData,val gst:String,val gstPesentage:String):Serializable
