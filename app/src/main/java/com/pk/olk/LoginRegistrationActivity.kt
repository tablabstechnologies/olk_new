package com.pk.olk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.pk.olk.databinding.ActivityLoginRegistrationBinding
import com.pk.olk.onclick.OnClicks


class LoginRegistrationActivity : AppCompatActivity() {
    lateinit var bindings:ActivityLoginRegistrationBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindings=DataBindingUtil.setContentView(this,R.layout.activity_login_registration)

        bindings.btnLogIn.setOnClickListener {
            startActivity(Intent(this,LoginActivity::class.java))
        }

        bindings.btnSignUp.setOnClickListener {
            startActivity(Intent(this,RegisterActivity::class.java))
        }
    }

    override fun onBackPressed() {
//        super.onBackPressed()
        finishAffinity()
    }
}