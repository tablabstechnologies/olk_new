package com.pk.olk


import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.text.format.Time
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.gson.JsonObject
import com.pk.olk.databinding.ActivityAddCompanyBinding
import com.pk.olk.model.GenericModelClass
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Pattern
import com.pk.olk.utils.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


class AddCompanyActivity : AppCompatActivity() {

    lateinit var binding: ActivityAddCompanyBinding
    var selectedState: String? = null

    val listStateName= ArrayList<String>()
    val listStateCode= ArrayList<String>()
    var startDate:String="";
    var endDate:String="";



    //    val states = resources.getStringArray(R.array.state)

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_company)
        callState()

        binding.states.setAdapter(
            ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                listStateName
            )
        )

        binding.btnSave.setOnClickListener {

            checkValid();
        }
        binding.yearStart.setOnClickListener {
            callCalender()
        }
        binding.registerDate.setOnClickListener {

            callRegiCalender()
        }
        binding.appBar.setOnClickListener {
            finish()
        }

        /*binding.states.onItemClickListener =
                AdapterView.OnItemClickListener{parent,view,position,id ->
                    selectedState = parent.getItemAtPosition(position).toString()
                }*/
        binding.states.onItemClickListener =
                AdapterView.OnItemClickListener { parent, view, position, id ->
                    selectedState = parent.getItemAtPosition(position).toString()
                    // Display the clicked item using toast

                }

        //var odt = OffsetDateTime.parse(dateString);
        //var dtf = DateTimeFormatter.ofPattern("MMM dd, uuuu", Locale.ENGLISH);
        // System.out.println(dtf.format(odt));




    }

    private fun callState() {
            val states="{\"gkstatus\": 0, \"gkresult\": [{\"statecode\": 35, \"statename\": \"Andaman and Nicobar Islands\"}, {\"statecode\": 28, \"statename\": \"Andhra Pradesh\"}, {\"statecode\": 37, \"statename\": \"Andhra Pradesh (New)\"}, {\"statecode\": 12, \"statename\": \"Arunachal Pradesh\"}, {\"statecode\": 18, \"statename\": \"Assam\"}, {\"statecode\": 10, \"statename\": \"Bihar\"}, {\"statecode\": 4, \"statename\": \"Chandigarh\"}, {\"statecode\": 22, \"statename\": \"Chhattisgarh\"}, {\"statecode\": 26, \"statename\": \"Dadra and Nagar Haveli\"}, {\"statecode\": 25, \"statename\": \"Daman and Diu\"}, {\"statecode\": 7, \"statename\": \"Delhi\"}, {\"statecode\": 30, \"statename\": \"Goa\"}, {\"statecode\": 24, \"statename\": \"Gujarat\"}, {\"statecode\": 6, \"statename\": \"Haryana\"}, {\"statecode\": 2, \"statename\": \"Himachal Pradesh\"}, {\"statecode\": 1, \"statename\": \"Jammu and Kashmir\"}, {\"statecode\": 20, \"statename\": \"Jharkhand\"}, {\"statecode\": 29, \"statename\": \"Karnataka\"}, {\"statecode\": 32, \"statename\": \"Kerala\"}, {\"statecode\": 31, \"statename\": \"Lakshdweep\"}, {\"statecode\": 23, \"statename\": \"Madhya Pradesh\"}, {\"statecode\": 27, \"statename\": \"Maharashtra\"}, {\"statecode\": 14, \"statename\": \"Manipur\"}, {\"statecode\": 17, \"statename\": \"Meghalaya\"}, {\"statecode\": 15, \"statename\": \"Mizoram\"}, {\"statecode\": 13, \"statename\": \"Nagaland\"}, {\"statecode\": 21, \"statename\": \"Odisha\"}, {\"statecode\": 34, \"statename\": \"Pondicherry\"}, {\"statecode\": 3, \"statename\": \"Punjab\"}, {\"statecode\": 8, \"statename\": \"Rajasthan\"}, {\"statecode\": 11, \"statename\": \"Sikkim\"}, {\"statecode\": 33, \"statename\": \"Tamil Nadu\"}, {\"statecode\": 36, \"statename\": \"Telangana\"}, {\"statecode\": 16, \"statename\": \"Tripura\"}, {\"statecode\": 9, \"statename\": \"Uttar Pradesh\"}, {\"statecode\": 5, \"statename\": \"Uttarakhand\"}, {\"statecode\": 19, \"statename\": \"West Bengal\"}]}"
            val ob= JSONObject(states)
            val array=ob.getJSONArray("gkresult")
            for (i in 0..array.length()-1) {
                val code=  array.getJSONObject(i).getString("statecode")
                val name=  array.getJSONObject(i).getString("statename")
                listStateName.add(name)
                listStateCode.add(code)

            }
//        TODO("Not yet implemented")
    }


    private fun callRegiCalender() {
        val calendar: Calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        val dateDlg = DatePickerDialog(
            this@AddCompanyActivity,
            { view, year, monthOfYear, dayOfMonth ->
                var strDate: CharSequence? = null
                val chosenDate = Time()
                chosenDate.set(dayOfMonth, monthOfYear, year)
                val dtDob: Long = chosenDate.toMillis(true)
                strDate = android.text.format.DateFormat.format("yyyy-MM-dd", dtDob)
                binding.registerDate.setText(strDate)
                Log.d("parseTesting", strDate.toString())
            }, year, month, day
        )
        dateDlg.show()

    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun callCalender() {
        val calendar: Calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)


        val dateDlg = DatePickerDialog(
            this@AddCompanyActivity,
            { view, year, monthOfYear, dayOfMonth ->
                var strDate: CharSequence? = null
                val chosenDate = Time()
                val chosenNextDate = Time()

                chosenDate.set(dayOfMonth, monthOfYear, year)
                chosenNextDate.set(dayOfMonth, (monthOfYear - 1), (year + 1))
                val dtDob: Long = chosenDate.toMillis(true)
                strDate = android.text.format.DateFormat.format("dd-MM-yyyy", dtDob)
                binding.yearStart.setText(strDate)

                val dtDobNext: Long = chosenNextDate.toMillis(true)
                strDate = android.text.format.DateFormat.format("dd-MM-yyyy", dtDobNext)
                endDate= android.text.format.DateFormat.format("dd-MM-yyyy", dtDobNext).toString()
                binding.yearEnd.setText(strDate)


            }, year, month, day
        )

        dateDlg.show()
    }

    private fun checkValid() {
        if (!TextUtils.isEmpty(binding.orgName.text.toString())) {

            if (!TextUtils.isEmpty(binding.yearStart.text.toString())) {

               // if (validation()){
                   if(checkValidGST()){

                       //if(checkEmail()){
                           callApi()
                       //}
                   }
               // }
            } else {
                CustomeToast.Show(this, "Please Select start Date ")
            }

        } else {
            CustomeToast.Show(this, "Please Enter Name of Company")
        }
    }

    /*private fun checkEmail():Boolean{
        if(!TextUtils.isEmpty(binding.email.text.toString())){
            if(Pattern.regex_matcher(Pattern.EMAIL,binding.email.text.toString())){
                return true
            }else{
                CustomeToast.Show(this,"Please Enter Valid Email")
                return false
            }
        }else{
            return true
        }
    }
*/
    private fun checkValidGST(): Boolean {
        if(!TextUtils.isEmpty(binding.gstNumber.text.toString())){
            if(Pattern.regex_matcher(Pattern.GSTIN, binding.gstNumber.text.toString())){
                return true
            }else{
                CustomeToast.Show(this, "Please Enter Valid GST Number")
                return false
            }
        }else{
            return true
        }
    }

/*    private fun validation(): Boolean {
        if(!TextUtils.isEmpty(binding.panNumber.text.toString())){
            if(Pattern.regex_matcher(Pattern.panPatter,binding.panNumber.text.toString())){
                return true
            }else{
                CustomeToast.Show(this,"Please Enter Valid PAN Number")
                return false
            }



        }else{
            return true
        }


    }*/

   /* private fun showdate(){
       *//* val tempdt1 =   binding.yearEnd.text.toString();
        val tempdt2 =   binding.yearStart.text.toString();
        val df = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter.ofPattern("dd-MM-yyyy")
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        val d1 = LocalDate.parse(tempdt1, df)
        val d2 = LocalDate.parse(tempdt2, df)
        Log.d("Firstcheck", d2.toString())
        Log.d("Firstcheck", d1.toString())*//*
    }*/

    private fun callApi() {
        val tempdt1 =   binding.yearStart.text.toString();
        val tempdt2 =   binding.yearEnd.text.toString();
        val df = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter.ofPattern("dd-MM-yyyy")
        } else {
            TODO("VERSION.SDK_INT < O")
        }
        val d1 = LocalDate.parse(tempdt1, df)
        val d2 = LocalDate.parse(tempdt2, df)
       /* Log.d("Firstcheck", d2.toString())
        Log.d("Firstcheck", d1.toString())*/


        val objects: JsonObject = JsonObject()
        objects.addProperty("orgname", binding.orgName.text.toString())
        //objects.addProperty("yearstart", binding.yearStart.text.toString())
        //objects.addProperty("yearend", binding.yearEnd.text.toString())
        objects.addProperty("yearstart", d1.toString())
        objects.addProperty("yearend", d2.toString())
        //objects.addProperty("orgregno", binding.contact.text.toString())
       // objects.addProperty("orgregdate", binding.registerDate.text.toString())
        objects.addProperty("orgtelno", binding.registerNumber.text.toString())
        //objects.addProperty("orgpan", binding.panNumber.text.toString())
        //objects.addProperty("orgemail", binding.email.text.toString())
        //objects.addProperty("orgwebsite", binding.webLink.text.toString())
        objects.addProperty("orggstin", binding.gstNumber.text.toString())
        objects.addProperty("orgtype", "Profit Making")
        objects.addProperty("billflag", 1)
        objects.addProperty("invflag", 1)
        objects.addProperty("invsflag", 1)
        objects.addProperty("avflag", 1)

    //cnew changes
    objects.addProperty("orgaddr", binding.address.text.toString())
    objects.addProperty("orgstate", selectedState)
        println("the resquest body is "+objects.toString())

        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val token: String = Utils.getLoginData(this)!!
        val call = SaasCall.apiInterface.createcompany(token, body)

        val progressDailog: ProgressDialog = ProgressDialog(this)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()
        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
//                println("The Response is " + response.body()?.gkstatus)
//                println("The Response is " + response.code())
                progressDailog.dismiss()

                if (response.body()?.gkstatus.equals("0")) {

                    Utils.setLoginResponse(response.body()?.token, this@AddCompanyActivity)
                    finish()


                } else {
                    CustomeToast.Show(this@AddCompanyActivity, "Failed to save data")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@AddCompanyActivity, "User Already Found")
                println("The Response is Fail;")
                println("The Response is Fail")
                progressDailog.dismiss()

            }

        })
    }


}