package com.pk.olk

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.pk.olk.bottosheet.AddBottomSheet
import com.pk.olk.databinding.ActivityMoreBinding
import com.pk.olk.retrofit.ApiCallBack.serviceSetterGetter
import com.pk.olk.utils.Utils


class MoreActivity : AppCompatActivity() {
    lateinit var binding:ActivityMoreBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_more)
        binding.textName.setText(Utils.getCompanyName(this))
       /* binding.textOwnPost.setOnClickListener {

           val intent= Intent(this,CustomerListActivity::class.java)
            intent.putExtra("data","1")
            startActivity(intent)

        }*/

      /*  binding.suplier.setOnClickListener {

            val intent= Intent(this,CustomerListActivity::class.java)
            intent.putExtra("data","2")
            startActivity(intent)

        }*/


 /*binding.product.setOnClickListener {

            val intent= Intent(this,ProductListActivity::class.java)
            //intent.putExtra("data","2")
            startActivity(intent)

        }*/
        binding.logout.setOnClickListener {
           // applicationContext.getSharedPreferences("LoginData", Context.MODE_PRIVATE).edit().clear().apply()
            val preferences = applicationContext.getSharedPreferences("LoginData",
                    Context.MODE_PRIVATE)
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            //startActivity(Intent(this,LoginActivity::class.java))

            startActivity(Intent(this,LoginRegistrationActivity::class.java))
            val toast: Toast =
                    Toast.makeText(this, "Logout successfully", Toast.LENGTH_LONG)
            toast.show()
            finish()
        }

        /*binding.invoice.setOnClickListener {

            val intent= Intent(this,GenerateInvoiceActivity::class.java)
            //intent.putExtra("data","2")
            startActivity(intent)

        }
        binding.party.setOnClickListener {
            val intent = Intent (this,AddCustomerActivity::class.java)
            startActivity(intent)
        }
        binding.productsBottom.setOnClickListener {
            val intent = Intent(this,AddProductActivity::class.java)
            startActivity(intent)
        }*/




        binding.navBar.imgPos.setOnClickListener {
            val bootomSheet= AddBottomSheet()
            bootomSheet.show(supportFragmentManager,"back")

        }
        binding.navBar.invoice.setOnClickListener {
            startActivity(Intent(this,GenerateInvoiceActivity::class.java))
        }
        binding.navBar.party.setOnClickListener {
            startActivity(Intent(this,AddCustomerActivity::class.java))
        }
        binding.navBar.products.setOnClickListener {
            startActivity(Intent(this,AddProductActivity::class.java))
        }



    }
}