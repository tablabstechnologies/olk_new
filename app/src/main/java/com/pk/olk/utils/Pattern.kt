package com.pk.olk.utils

import java.util.regex.Matcher
import java.util.regex.Pattern

object Pattern {
    val  panPatter:String="(([A-Za-z]{5})([0-9]{4})([a-zA-Z]))"
    val GSTIN="^([0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1})\$"
    //val GSTIN="^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+\$"
    /*val EMAIL="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"*/

    val EMAIL="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    val MOBILE="^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"
    val IFSC="^[A-Z]{4}0[A-Z0-9]{6}$"



     fun regex_matcher(patternString: String, string: String): Boolean {
//        val pattern = Pattern.compile(patternString)
//        val m: Matcher = pattern.matcher(string)
//        return m.find() && m.group(0) != null

        return string.matches(patternString.toRegex())
    }

}