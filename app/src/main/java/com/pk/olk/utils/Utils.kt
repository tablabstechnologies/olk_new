package com.pk.olk.utils

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.format.DateTimeParseException
import java.util.regex.Matcher
import java.util.regex.Pattern


object Utils {

    fun setLoginResponse(
        responces: String?,
        context: Context
    ) {
        val preferences = context.getSharedPreferences(
            "LoginData",
            Context.MODE_PRIVATE
        )
        val editor = preferences.edit()
        editor.putString("LoginData", responces)
        editor.commit()
    }
    fun getLoginData(context: Context): String? {
        val preferences =
            context.getSharedPreferences("LoginData", Context.MODE_PRIVATE)
        val data = preferences.getString("LoginData", null)
        val gson = Gson()
        if(data==null){
            return null
        }else{
           return data
        }

    }


    fun setCompanyResponse(
        responces: String?,
        context: Context
    ) {
        val preferences = context.getSharedPreferences(
            "CompanyToken",
            Context.MODE_PRIVATE
        )
        val editor = preferences.edit()
        editor.putString("CompanyToken", responces)
        editor.commit()
    }
    fun getCompanyData(context: Context): String? {
        val preferences =
            context.getSharedPreferences("CompanyToken", Context.MODE_PRIVATE)
        val data = preferences.getString("CompanyToken", null)

        if(data==null){
            return null
        }else{
            return data
        }

    }


    fun setCompanyName(
        responces: String?,
        context: Context
    ) {
        val preferences = context.getSharedPreferences(
            "CompanyToken",
            Context.MODE_PRIVATE
        )
        val editor = preferences.edit()
        editor.putString("Name", responces)
        editor.commit()
    }
    fun getCompanyName(context: Context): String? {
        val preferences =
            context.getSharedPreferences("CompanyToken", Context.MODE_PRIVATE)
        val data = preferences.getString("Name", null)

        if(data==null){
            return null
        }else{
            return data
        }

    }




    @RequiresApi(Build.VERSION_CODES.O)
    fun getDate(dates:String):String?{

        val f = DateTimeFormatterBuilder().parseCaseInsensitive()
            .append(DateTimeFormatter.ofPattern("yyyy-MMM-dd")).toFormatter()
        try {
            val datetime = LocalDate.parse(dates, f)
            println("date is "+datetime) // 2019-12-22
            return datetime.toString()
        } catch (e: DateTimeParseException) {
            // Exception handling message/mechanism/logging as per company standard
            e.printStackTrace()
        }
        return null
    }

    //newedits
    fun clearLogin(
            responces: String?,
            context: Context
    ) {
        val preferences = context.getSharedPreferences(
                "LoginData",
                Context.MODE_PRIVATE
        )
        val editor = preferences.edit()
        editor.putString("LoginData", responces)
        editor.clear();
        editor.commit()

    }



}