package com.pk.olk

import CompanyData
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.pk.olk.adapter.CompanyListAdapter
import com.pk.olk.databinding.ActivityCompanyListBinding
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CompanyListActivity : AppCompatActivity() {
    lateinit var binding: ActivityCompanyListBinding
    lateinit var token: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_company_list)
        token= Utils.getLoginData(this)!!
        binding.recView.layoutManager = LinearLayoutManager(this)

        binding.flotAddCompany.setOnClickListener {

            startActivity(Intent(this, AddCompanyActivity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()
        callApi()

    }

    private fun callApi() {

        try {


            val call = SaasCall.apiInterface.accountProile(token)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<CompanyData> {
                override fun onResponse(
                    call: Call<CompanyData>,
                    response: Response<CompanyData>
                ) {
                     progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {
                        response.body()!!.profile.complist

                        val listAdpter =
                            CompanyListAdapter(
                                this@CompanyListActivity,
                                response.body()!!.profile.complist
                            )

                        binding.recView.adapter = listAdpter
                        if(response!!.body()!!.profile.complist.size==0){
                            startActivity(Intent(this@CompanyListActivity, AddCompanyActivity::class.java))
                        }


                    } else {
                        CustomeToast.Show(this@CompanyListActivity, "User Already Found")

                    }

                }

                override fun onFailure(call: Call<CompanyData>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@CompanyListActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e:Exception) {

            e.printStackTrace()
        }
    }
}
