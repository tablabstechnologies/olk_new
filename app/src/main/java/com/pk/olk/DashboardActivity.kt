package com.pk.olk

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.pk.olk.bottosheet.AddBottomSheet
import com.pk.olk.databinding.ActivityDashboradOneBinding
import com.pk.olk.utils.Utils


class DashboardActivity : AppCompatActivity() {

    lateinit var binding:ActivityDashboradOneBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil. setContentView(this,R.layout.activity_dashborad_one)


      /*  binding. imgPosAdd.setOnClickListener {

            val bootomSheet=AddBottomSheet()
            bootomSheet.show(supportFragmentManager,"back")

        }*/
        binding.navBar.imgPos.setOnClickListener {
            val bootomSheet=AddBottomSheet()
            bootomSheet.show(supportFragmentManager,"back")
        }

        binding.appBar.nameCompany.setText(Utils.getCompanyName(this))

       binding.appBar.chageComp.setOnClickListener {

            startActivity(Intent(this,CompanyListActivity::class.java))
        }

        binding.appBar.more.setOnClickListener {
            startActivity(Intent(this,MoreActivity::class.java))
        }

        binding.navBar.invoice.setOnClickListener {
            startActivity(Intent(this,GenerateInvoiceActivity::class.java))
        }
        binding.navBar.party.setOnClickListener {
            startActivity(Intent(this,AddCustomerActivity::class.java))
        }
        binding.navBar.products.setOnClickListener {
            startActivity(Intent(this,AddProductActivity::class.java))
        }

    }
}