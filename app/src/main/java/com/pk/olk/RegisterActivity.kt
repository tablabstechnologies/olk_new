package com.pk.olk

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import androidx.databinding.DataBindingUtil
import com.google.gson.JsonObject
import com.pk.olk.databinding.ActivityRegisterBinding
import com.pk.olk.model.GenericModelClass
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Pattern
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {
  lateinit var   binding:ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_register)

        binding.btnLogIn.setOnClickListener {

            checkCodition()
        }
    }

    private fun checkCodition() {
        if (!TextUtils.isEmpty(binding.name.text.toString())){
            if (!TextUtils.isEmpty(binding.email.text.toString())){
                if (!TextUtils.isEmpty(binding.contact.text.toString())){
                    if (!TextUtils.isEmpty(binding.password.text.toString())){
                        if (!TextUtils.isEmpty(binding.confrimPassword.text.toString())){
                          /*  if (!TextUtils.isEmpty(binding.nameCompany.text.toString())){
                                if (!TextUtils.isEmpty(binding.natureBusiness.text.toString())){*/
                                    if(Pattern.regex_matcher(Pattern.EMAIL,binding.email.text.toString())){

                                        if(Pattern.regex_matcher(Pattern.MOBILE,binding.contact.text.toString())){
                                            callApi()
                                        }else{
                                            CustomeToast.Show(this,"Please Enter Valid Mobile Number")
                                        }
                                    }else{
                                        CustomeToast.Show(this,"Please Enter Valid Email")
                                    }
                             /*   }else{
                                    CustomeToast.Show(this,"Please Enter Nature Business")
                                }

                            }else{
                                CustomeToast.Show(this,"Please Enter Name Company")
                            }*/
                        }else{
                            CustomeToast.Show(this,"Please Enter Confrim Password")
                        }

                    }else{
                        CustomeToast.Show(this,"Please Enter password")
                    }
                }else{
                    CustomeToast.Show(this,"Please Enter contact")
                }

            }else{
                CustomeToast.Show(this,"Please Enter Email")
            }
        }else{
            CustomeToast.Show(this,"Please Enter Name")
        }
    }

    private fun callApi() {

        val objects:JsonObject= JsonObject()
        objects.addProperty("email",binding.email.text.toString())
        objects.addProperty("username",binding.name.text.toString())

        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val call = SaasCall.apiInterface.sendVeriicationCode(body)

        val progressDailog: ProgressDialog = ProgressDialog(this)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()


        call.enqueue(object : Callback<GenericModelClass>{
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>){
                println("The Response is "+response.body()?.gkstatus)
                println("The Response is "+response.code())
                progressDailog.dismiss()

                if(response.body()?.gkstatus.equals("0")){
                    callNextScreen()

                }else{
                    CustomeToast.Show(this@RegisterActivity,"User Already Found")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@RegisterActivity,"User Already Found")
                println("The Response is Fail")
                progressDailog.dismiss()

            }
        })
    }

    private fun callNextScreen() {
        val intent:Intent= Intent(this,OTPActivity::class.java)
        intent.putExtra("email",binding.email.text.toString())
        intent.putExtra("name",binding.name.text.toString())
        intent.putExtra("password",binding.password.text.toString())
        intent.putExtra("orgType","3")
       // intent.putExtra("orgName",binding.nameCompany.text.toString()) // this
        intent.putExtra("contact",binding.contact.text.toString())
        startActivity(intent)
    }
}