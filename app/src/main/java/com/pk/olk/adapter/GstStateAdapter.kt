package com.pk.olk.adapter

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.pk.olk.DashboardActivity
import com.pk.olk.R
import com.pk.olk.model.GenericModelClass
import com.pk.olk.model.StateGst
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GstStateAdapter(val context: Context,val listGst:ArrayList<StateGst>,val addState:AddState) :
RecyclerView.Adapter<GstStateAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
//        val name: TextView =itemView.findViewById(R.id.name)
//        val number: TextView =itemView.findViewById(R.id.number)



    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GstStateAdapter.ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_gst,null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: GstStateAdapter.ViewHolder, position: Int) {

//        holder.name.setText(listGst.get(position).stateName)
//        holder.number.setText(listGst.get(position).Gst)



    }



    override fun getItemCount(): Int {
        return listGst.size
    }

    interface AddState{
        fun addedState()
    }

}