package com.pk.olk.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pk.olk.R
import com.pk.olk.model.ProductData

class ProductAdapter(val context: Context, val listGst: ArrayList<ProductData>) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val Unit: TextView = itemView.findViewById(R.id.Unit)
        val stock: TextView = itemView.findViewById(R.id.stock)


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_product, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name.setText(listGst.get(position).productdesc)
        holder.Unit.setText("Unit : " + listGst.get(position).unitname)
        holder.Unit.setText("stock : " + listGst.get(position).productquantity)


    }


    override fun getItemCount(): Int {
        return listGst.size
    }
}