package com.pk.olk.adapter

import Complist
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.pk.olk.DashboardActivity
import com.pk.olk.R
import com.pk.olk.model.GenericModelClass
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompanyListAdapter ( val context: Context, val listCompany:List<Complist>) :
    RecyclerView.Adapter<CompanyListAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val textCompanyName:TextView=itemView.findViewById(R.id.textCompanyName)
        val textviewDate:TextView=itemView.findViewById(R.id.textviewDate)
        val paidStatus:TextView=itemView.findViewById(R.id.paidStatus)
        val textTag:TextView=itemView.findViewById(R.id.textTag)


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyListAdapter.ViewHolder {
        val view:View=LayoutInflater.from(context).inflate(R.layout.item_company,null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CompanyListAdapter.ViewHolder, position: Int) {
        holder.textCompanyName.setText(listCompany.get(position).orgname)
        holder.textviewDate.setText(listCompany.get(position).year1+" "+listCompany.get(position).year1)
        //holder.paidStatus.setText(listCompany.get(position).)
        holder.itemView.setOnClickListener {

            callApi(position);

        }


    }

    private fun callApi(position: Int) {
        val objects: JsonObject = JsonObject()
        objects.addProperty("orgcode", listCompany.get(position).orgcode)



        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val token: String = Utils.getLoginData(context)!!
        val call = SaasCall.apiInterface.gkLogin(token, body)

        val progressDailog: ProgressDialog = ProgressDialog(context)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()
        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
                println("The Response is " + response.body()?.gktoken)
                println("The Response is " + response.code())
                progressDailog.dismiss()

                if (response.body()?.gkstatus.equals("0")) {

                    Utils.setCompanyResponse(response.body()?.gktoken,context)
                    Utils.setCompanyName(listCompany.get(position).orgname,context)
                    context.startActivity(Intent(context,DashboardActivity::class.java))



                } else {
                    CustomeToast.Show(context, "Not Valid Entry ")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(context, "User Already Found")
                println("The Response is Fail;")
                println("The Response is Fail")
                progressDailog.dismiss()

            }

        })




    }

    override fun getItemCount(): Int {
        return listCompany.size
    }
}