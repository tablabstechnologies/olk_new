package com.pk.olk.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pk.olk.InvoiceProductAddActivity
import com.pk.olk.R
import com.pk.olk.model.ProductAdd
import com.pk.olk.model.StateGst
import interfaces.EditItem

class AddProductAdapter(val context: Context, val listGst: ArrayList<ProductAdd>,val editItem: EditItem) :
    RecyclerView.Adapter<AddProductAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val quntity: TextView = itemView.findViewById(R.id.quntity)
        val discountAmount: TextView = itemView.findViewById(R.id.discountAmount)
        val totalAmount: TextView = itemView.findViewById(R.id.totalAmount)
 val delete: ImageView = itemView.findViewById(R.id.delete)


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.item_add_product, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.name.setText(listGst.get(position).productData.productdesc)
        holder.quntity.setText(listGst.get(position).totalQuntity)
        holder.discountAmount.setText(listGst.get(position).discount)
        holder.totalAmount.setText(listGst.get(position).totalAount)

        holder.itemView.setOnClickListener {

          editItem.editItemPrduct(position)
        }

 holder.delete.setOnClickListener {

          editItem.deleteItem(position)
        }




    }


    override fun getItemCount(): Int {
        return listGst.size
    }
}