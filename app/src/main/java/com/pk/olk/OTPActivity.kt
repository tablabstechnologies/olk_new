package com.pk.olk

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.gson.JsonObject
import com.pk.olk.databinding.ActivityOtpactivityBinding
import com.pk.olk.model.GenericModelClass
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OTPActivity : AppCompatActivity() {
    lateinit var binding: ActivityOtpactivityBinding
    lateinit var email: String
    lateinit var name: String
    lateinit var password: String
    lateinit var orgType: String
    //lateinit var orgName: String
    lateinit var contact: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otpactivity)

        email = intent.getStringExtra("email")!!
        name = intent.getStringExtra("name")!!
        password = intent.getStringExtra("password")!!
        orgType = intent.getStringExtra("orgType")!!
       // orgName = intent.getStringExtra("orgName")!!
        contact = intent.getStringExtra("contact")!!

        binding.regiEmailId.setText("OTP is sent to "+email)


        binding.btnLogIn.setOnClickListener {

            if (!TextUtils.isEmpty(binding.password.text.toString())) {

                callAPI()
            } else {
                CustomeToast.Show(this, "Enter OTP")
            }
        }
    }

    private fun callAPI() {
       val objects: JsonObject = JsonObject()
        objects.addProperty("email", email)
        objects.addProperty("code", binding.password.text.toString())

        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val call = SaasCall.apiInterface.matchcode(body)

        val progressDailog: ProgressDialog = ProgressDialog(this)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()

        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
                println("The Response is " + response.body()?.gkstatus)
                println("The Response is " + response.code())
                progressDailog.dismiss()
                if (response.body()?.gkstatus.equals("0")) {
                    callRegister()

                } else {
                    CustomeToast.Show(this@OTPActivity, "OTP not Match")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@OTPActivity, "User Already Found")
                progressDailog.dismiss()

            }

        })
    }

    private fun callRegister() {

        val objects: JsonObject = JsonObject()
        objects.addProperty("accountname", name)
        objects.addProperty("accountemail", email)
        objects.addProperty("accountpassword",password)
        objects.addProperty("accounttelno", contact)
        objects.addProperty("businesstype", "Services")
        //objects.addProperty("companyname", orgName)


        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )

        val call = SaasCall.apiInterface.register(body)

        val progressDailog: ProgressDialog = ProgressDialog(this)
        progressDailog.setCancelable(false)
        progressDailog.setMessage("Loading....")
        progressDailog.show()
        call.enqueue(object : Callback<GenericModelClass> {
            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
                println("The Response is " + response.body()?.gkstatus)
                println("The Response is " + response.code())
                progressDailog.dismiss()

                if (response.body()?.gkstatus.equals("0")) {

                    Utils.setLoginResponse(response.body()?.token,this@OTPActivity)

                    startActivity(Intent(this@OTPActivity,CompanyListActivity::class.java))
                    finish()



                } else {
                    CustomeToast.Show(this@OTPActivity, "OTP not Match")

                }
            }

            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                //TODO("Not yet implemented")
                CustomeToast.Show(this@OTPActivity, "User Already Found")
                println("The Response is Fail;")
                println("The Response is Fail")
                progressDailog.dismiss()

            }

        })

    }


}