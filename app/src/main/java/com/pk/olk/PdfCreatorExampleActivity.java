package com.pk.olk;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.print.PDFPrint;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.text.HtmlCompat;

import com.pk.olk.model.ProductAdd;
import com.pk.olk.model.ProductData;
import com.pk.olk.utils.Utils;
import com.tejpratapsingh.pdfcreator.activity.PDFCreatorActivity;
import com.tejpratapsingh.pdfcreator.utils.FileManager;
import com.tejpratapsingh.pdfcreator.utils.PDFUtil;
import com.tejpratapsingh.pdfcreator.views.PDFBody;
import com.tejpratapsingh.pdfcreator.views.PDFFooterView;
import com.tejpratapsingh.pdfcreator.views.PDFHeaderView;
import com.tejpratapsingh.pdfcreator.views.PDFTableView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFHorizontalView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFImageView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFLineSeparatorView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFPageBreakView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFTextView;
import com.tejpratapsingh.pdfcreator.views.basic.PDFVerticalView;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class PdfCreatorExampleActivity extends PDFCreatorActivity {

    ArrayList<ProductAdd>productAddArrayList;
    double tot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        productAddArrayList= (ArrayList<ProductAdd>) getIntent().getSerializableExtra("data");
        tot=getIntent().getDoubleExtra("tot",0);

//        productAddArrayList=new ArrayList<>();
//        ProductData productData=new ProductData(1,"unit","cat",1,"Laptop","text","20",10);
//        ProductAdd productAdd=new ProductAdd("200","10","10","10","50",productData,"5%","3");
//        productAddArrayList.add(productAdd);
//
////        ProductData productDatas=new ProductData(1,"unit","cat",1,"Laptop Lenovo","text","20",10);
////        ProductAdd productAdds=new ProductAdd("200","10","10","10","50",productDatas,"5%","3");
////        productAddArrayList.add(productAdds);

//
//        createPDF("tests", new PDFUtil.PDFUtilListener() {
//            @Override
//            public void pdfGenerationSuccess(File savedPDFFile) {
//                Toast.makeText(PdfCreatorExampleActivity.this, "PDF Created", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void pdfGenerationFailure(Exception exception) {
//                Toast.makeText(PdfCreatorExampleActivity.this, "PDF NOT Created", Toast.LENGTH_SHORT).show();
//            }
//        });



        final File savedPDFFile = FileManager.getInstance().createTempFile(getApplicationContext(), "pdf", false);
// Generate Pdf From Html
        PDFUtil.generatePDFFromHTML(getApplicationContext(), savedPDFFile, "<!DOCTYPE html>\n" +
                format(), new PDFPrint.OnPDFPrintListener() {
            @Override
            public void onSuccess(File file) {
                // Open Pdf Viewer
                Uri pdfUri = Uri.fromFile(savedPDFFile);

                Intent intentPdfViewer = new Intent(PdfCreatorExampleActivity.this, PdfViewerExampleActivity.class);
                intentPdfViewer.putExtra(PdfViewerExampleActivity.PDF_FILE_URI, pdfUri);

                startActivity(intentPdfViewer);
            }

            @Override
            public void onError(Exception exception) {
                exception.printStackTrace();
            }
        });

    }

    @Override
    protected PDFHeaderView getHeaderView(int pageIndex) {
//        PDFHeaderView headerView = new PDFHeaderView(getApplicationContext());
//
//        PDFHorizontalView horizontalView = new PDFHorizontalView(getApplicationContext());
//
//        PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
//        SpannableString word = new SpannableString("TAX INVOICE - ORIGINAL FOR RECIPIENT");
//        word.setSpan(new ForegroundColorSpan(Color.BLACK), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        pdfTextView.setText(word);
//        pdfTextView.getView().setGravity(Gravity.CENTER);
//        pdfTextView.setLayout(new LinearLayout.LayoutParams(
//                0,
//                LinearLayout.LayoutParams.MATCH_PARENT, 1));
//        pdfTextView.getView().setGravity(Gravity.CENTER);
//        pdfTextView.getView().setTypeface(pdfTextView.getView().getTypeface(), Typeface.BOLD);
//
//        horizontalView.addView(pdfTextView);
//
////        PDFImageView imageView = new PDFImageView(getApplicationContext());
////        LinearLayout.LayoutParams imageLayoutParam = new LinearLayout.LayoutParams(
////                60,
////                60, 0);
////        imageView.setImageScale(ImageView.ScaleType.CENTER_INSIDE);
////        imageView.setImageResource(R.mipmap.ic_launcher);
////        imageLayoutParam.setMargins(0, 0, 10, 0);
////        imageView.setLayout(imageLayoutParam);
////
////        horizontalView.addView(imageView);
////
//       headerView.addView(horizontalView);
//
//        PDFLineSeparatorView lineSeparatorView1 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
//        headerView.addView(lineSeparatorView1);
//
//        return headerView;
    return  null;
    }

    @Override
    protected PDFBody getBodyViews() {
        PDFBody pdfBody = new PDFBody();

        PDFTextView titleName=new PDFTextView(
                getApplicationContext(),PDFTextView.PDF_TEXT_SIZE.SMALL);
        titleName.getView().setText("TAX INVOICE - ORIGINAL FOR RECIPIENT");
        titleName.getView().setGravity(Gravity.CENTER);
        pdfBody.addView(titleName);



//        PDFTextView pdfCompanyNameView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H1);
//
//
//        pdfCompanyNameView.setText(Utils.INSTANCE.getCompanyName(this));
//        pdfBody.addView(pdfCompanyNameView);
//        PDFLineSeparatorView lineSeparatorView1 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
//        pdfBody.addView(lineSeparatorView1);
//        pdfBody.addView(lineSeparatorView1);
//        pdfBody.addView(lineSeparatorView1);
//
//
//        PDFTextView pdfAddressView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
//        pdfAddressView.setText("Address Line 1\nCity, State - 123456");
//
//
//        pdfBody.addView(pdfAddressView);


        PDFHorizontalView horizontalView = new PDFHorizontalView(getApplicationContext());
        PDFVerticalView eView = new PDFVerticalView(getApplicationContext());




        PDFTextView pdfCompanyNameView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H1);
        pdfCompanyNameView.setText(Utils.INSTANCE.getCompanyName(this));
//        pdfBody.addView(pdfCompanyNameView);
//        PDFLineSeparatorView lineSeparatorView1 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
//        pdfBody.addView(lineSeparatorView1);
//        pdfBody.addView(lineSeparatorView1);
//        pdfBody.addView(lineSeparatorView1);


        PDFTextView pdfAddressView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
        pdfAddressView.setText("Address Line 1\nCity, State - 123456");



        PDFImageView imageView = new PDFImageView(getApplicationContext());
        LinearLayout.LayoutParams imageLayoutParam = new LinearLayout.LayoutParams(
                60,
                60, 0);
        imageView.setImageScale(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setImageResource(R.mipmap.ic_launcher);
        imageLayoutParam.setMargins(0, 0, 10, 0);
        imageView.setLayout(imageLayoutParam);

        eView.addView(pdfCompanyNameView);
        eView.addView(pdfAddressView);

        horizontalView.addView(imageView);
        horizontalView.addView(eView);

        pdfBody.addView(horizontalView);


       // pdfBody.addView(pdfAddressView);









        PDFLineSeparatorView lineSeparatorView2 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        lineSeparatorView2.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                8, 0));
        pdfBody.addView(lineSeparatorView2);

        PDFLineSeparatorView lineSeparatorView3 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.WHITE);
        pdfBody.addView(lineSeparatorView3);

        int[] widthPercent = {17,17,17,17,16,15}; // Sum should be equal to 100%
        String[] textInTable = {"Product", "HSN/SAC", "Billed Qty", "Free Qty","Rate","Total"};
        PDFTextView pdfTableTitleView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
        pdfTableTitleView.setText("");
        pdfBody.addView(pdfTableTitleView);

//        final PDFPageBreakView pdfPageBreakView = new PDFPageBreakView(getApplicationContext());
//        pdfBody.addView(pdfPageBreakView);

        PDFVerticalView pdfVerticalView=new PDFVerticalView(getApplicationContext());
        pdfVerticalView.getView().setBackgroundResource(R.drawable.pdf_sperater);


        PDFTableView.PDFTableRowView tableHeader = new PDFTableView.PDFTableRowView(getApplicationContext());
        for (String s : textInTable) {
            PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);
            pdfTextView.setText("" + s);
            pdfTextView.setTextColor(Color.WHITE);
            tableHeader.addToRow(pdfTextView);
        }
        tableHeader.setBackgroundColor(Color.GRAY);


        PDFTableView.PDFTableRowView tableRowView1 = new PDFTableView.PDFTableRowView(getApplicationContext());

        PDFTableView tableView = new PDFTableView(getApplicationContext(), tableHeader, tableRowView1);

        PDFLineSeparatorView lineSeparatorView4 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.GRAY);

        lineSeparatorView4.setBackgroundColor(Color.BLACK);
        tableView.addSeparatorRow(lineSeparatorView4);



        //tableView.setLayout(new LinearLayout.LayoutParams())
        for (int i = 0; i < productAddArrayList.size(); i++) {
            // Create 10 rows
            PDFTableView.PDFTableRowView tableRowView = new PDFTableView.PDFTableRowView(getApplicationContext());

            for (String s : textInTable) {

                PDFTextView pdfTextView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.P);


                if (s.equalsIgnoreCase("Product")) {
                    pdfTextView.setText(productAddArrayList.get(i).getProductData().getProductdesc());

                }
                if (s.equalsIgnoreCase("HSN/SAC")) {
                    //  pdfTextView.setText(productAddArrayList.get(i).getProductData().getProductdesc());
                }
                if (s.equalsIgnoreCase("Billed Qty")) {
                    pdfTextView.setText(productAddArrayList.get(i).getTotalQuntity());
                }
                if (s.equalsIgnoreCase("Free Qty")) {
                    pdfTextView.setText(productAddArrayList.get(i).getFreeQuntity());
                }
                if (s.equalsIgnoreCase("Rate")) {
                    pdfTextView.setText(productAddArrayList.get(i).getRate());
                }
                if (s.equalsIgnoreCase("Total")) {
                    pdfTextView.setText(productAddArrayList.get(i).getTotalAount());
                }
                pdfTextView.getView().setCompoundDrawables(null,null,getDrawable(R.drawable.border_textview_lodin),null);
                tableRowView.addToRow(pdfTextView);

            }


            tableView.addRow(tableRowView);

            PDFLineSeparatorView lineSeparatorView5 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.GRAY);

            lineSeparatorView5.setBackgroundColor(Color.GRAY);

            tableView.addSeparatorRow(lineSeparatorView5);


        }



        tableView.setColumnWidth(widthPercent);

        pdfBody.addView(tableView);

     //   pdfVerticalView.addView(tableView);

//        PDFLineSeparatorView lineSeparatorView4 = new PDFLineSeparatorView(getApplicationContext()).setBackgroundColor(Color.BLACK);
//        pdfBody.addView(lineSeparatorView4);
//
//        PDFTextView pdfIconLicenseView = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.H3);
//        Spanned icon8Link = HtmlCompat.fromHtml("Icon from <a href='https://icons8.com'>https://icons8.com</a>", HtmlCompat.FROM_HTML_MODE_LEGACY);
//        pdfIconLicenseView.getView().setText(icon8Link);
//        pdfBody.addView(pdfIconLicenseView);

        return pdfBody;
    }

    @Override
    protected PDFFooterView getFooterView(int pageIndex) {
        PDFFooterView footerView = new PDFFooterView(getApplicationContext());

        PDFTextView pdfTextViewPage = new PDFTextView(getApplicationContext(), PDFTextView.PDF_TEXT_SIZE.SMALL);
        pdfTextViewPage.setText(String.format(Locale.getDefault(), "Page: %d", pageIndex + 1));
        pdfTextViewPage.setLayout(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT, 0));
        pdfTextViewPage.getView().setGravity(Gravity.CENTER_HORIZONTAL);

        footerView.addView(pdfTextViewPage);

        return footerView;
    }

//    @Nullable
//    @Override
//    protected PDFImageView getWatermarkView(int forPage) {
//        PDFImageView pdfImageView = new PDFImageView(getApplicationContext());
//        FrameLayout.LayoutParams childLayoutParams = new FrameLayout.LayoutParams(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                200, Gravity.CENTER);
//        pdfImageView.setLayout(childLayoutParams);
//
//        pdfImageView.setImageResource(R.drawable.olk_logo);
//        pdfImageView.setImageScale(ImageView.ScaleType.FIT_CENTER);
//        pdfImageView.getView().setAlpha(0.3F);
//
//        return pdfImageView;
//    }

    @Override
    protected void onNextClicked(final File savedPDFFile) {
        Uri pdfUri = Uri.fromFile(savedPDFFile);

        Intent intentPdfViewer = new Intent(PdfCreatorExampleActivity.this, PdfViewerExampleActivity.class);
        intentPdfViewer.putExtra(PdfViewerExampleActivity.PDF_FILE_URI, pdfUri);

        startActivity(intentPdfViewer);
    }

    String  format(){
        String comapnyName=Utils.INSTANCE.getCompanyName(this);
        return "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\n" +
                "<head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width\">\n" +
                "    <style>\n" +
                "        label {\n" +
                "            font-family: Arial, Helvetica, sans-serif;\n" +
                "        }\n" +
                "\n" +
                "        body {\n" +
                "            background-image: none;\n" +
                "            font-family: Arial, Helvetica, sans-serif;\n" +
                "        }\n" +
                "\n" +
                "        .page {\n" +
                "            width: 25cm;\n" +
                "            min-height: 29.7cm;\n" +
                "            padding: 1.5cm;\n" +
                "            margin: 1cm auto;\n" +
                "            border: 1px #D3D3D3 solid;\n" +
                "            border-radius: 5px;\n" +
                "            background: white;\n" +
                "            box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);\n" +
                "        }\n" +
                "\n" +
                "        .head-label {\n" +
                "            font-weight: bold;\n" +
                "            text-align: center;\n" +
                "        }\n" +
                "\n" +
                "        .logo {\n" +
                "            width: 120px;\n" +
                "            height: 120px;\n" +
                "            background-size: cover;\n" +
                "            background-position: top center;\n" +
                "            border-radius: 50%;\n" +
                "            padding: 5px;\n" +
                "            border: 1px solid darkgray;\n" +
                "        }\n" +
                "\n" +
                "        .card {\n" +
                "            box-shadow: 0 4px 8px 0 rgb(0 0 0 / 20%);\n" +
                "            border-radius: 5px;\n" +
                "            margin: 5% 12% 0px 12%;\n" +
                "            padding: 12px;\n" +
                "        }\n" +
                "\n" +
                "        .thead {\n" +
                "            background-color: rgb(49, 48, 48);\n" +
                "            color: white;\n" +
                "        }\n" +
                "\n" +
                "        .border-table {\n" +
                "            padding: 5px;\n" +
                "            border: 1px solid darkgray;\n" +
                "        }\n" +
                "\n" +
                "        .right {\n" +
                "            text-align: right;\n" +
                "        }\n" +
                "\n" +
                "        table {\n" +
                "            width: 100%;\n" +
                "            border-collapse: collapse;\n" +
                "        }\n" +
                "\n" +
                "        @page {\n" +
                "            size: A4 potrait;\n" +
                "            margin: 0;\n" +
                "        }\n" +
                "\n" +
                "        @media print {\n" +
                "            .page {\n" +
                "                margin: 0;\n" +
                "                padding: initial;\n" +
                "                border: initial;\n" +
                "                border-radius: initial;\n" +
                "                width: initial;\n" +
                "                min-height: initial;\n" +
                "                box-shadow: initial;\n" +
                "                background: initial;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        @media only screen and (max-width: 768px) {\n" +
                "\n" +
                "            /* For mobile phones: */\n" +
                "            .card {\n" +
                "                margin: 5% 2% 0px 2%;\n" +
                "            }\n" +
                "        }\n" +
                "\n" +
                "        div {\n" +
                "            font-size: 95%;\n" +
                "            font-family: Arial, Helvetica, sans-serif;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "    <div>\n" +
                "        <div class=\"card\">\n" +
                "            <div class=\"head-label\">\n" +
                "                <label><a>TAX INVOICE - ORIGINAL FOR RECIPIENT</a></label>\n" +
                "            </div>\n" +
                "            <table>\n" +
                "                <tr>\n" +
                "                    <td>\n" +
                "\n" +
                "                        <img class=\"logo\" id=\"imgbox2\" type=\"file\" src=\"http://i.stack.imgur.com/Dj7eP.jpg\">\n" +
                "\n" +
                "                    </td>\n" +
                "                    <td style=\"word-wrap: break-word; padding-left:25px !important;\">\n" +
                "\n" +
                "                        <h3>"+comapnyName+"</h3>\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">\n" +
                "                                Kalangar, Ahamdabad, Gujarat - 600033\n" +
                "                            </small> </label><br>\n" +
                "                        <label style=\"display:inline\">\n" +
                "                            <small style=\"font-weight : bold !Important; word-wrap: break-word\" ;> Contact No.:\n" +
                "                                767676676868</small>\n" +
                "                            <small style=\"font-weight : bold !Important; word-wrap: break-word\"> Email\n" +
                "                                :contact@gmail.com</small>\n" +
                "                        </label><br>\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\"> Website :\n" +
                "                                www.website.com</small></label><br>\n" +
                "                        <label> <small\n" +
                "                                style=\"font-weight : bold !Important; word-wrap: break-word\">GSTIN:24SDEF323Y234z3</small>\n" +
                "                        </label>\n" +
                "                    </td>\n" +
                "                    <br>\n" +
                "\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <table class=\"border-table\">\n" +
                "                <h4><small style=\"font-weight : bold !Important; word-wrap: break-word;\"> DETAILS Of INVOICE\n" +
                "                </h4>\n" +
                "                <tr>\n" +
                "                    <td style=\"word-wrap: break-word;\" class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">Invoice No:\n" +
                "                                </small>132F23234</label> &nbsp;&nbsp;\n" +
                "\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">Eway Bill\n" +
                "                                No.</small>989787</label>\n" +
                "                    </td>\n" +
                "                    <td class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">Invoice Date :\n" +
                "                               </small> 23-08-2323</label>\n" +
                "                    </td>\n" +
                "                    <td class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">State of Destination\n" +
                "                                :</small>Gujarat</label>\n" +
                "                    </td>\n" +
                "                    <td class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\"><b>State Code</b> :\n" +
                "                                </small>23 </label>\n" +
                "                    </td>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td style=\"word-wrap: break-word;\" class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\"><b>Customer</b>\n" +
                "                                : </small>Parag</label>\n" +
                "\n" +
                "                    </td>\n" +
                "                    <td class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">GSTIN\n" +
                "                                :</small>123123ER3</label>\n" +
                "                    </td>\n" +
                "                    <td class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\">Customer State\n" +
                "                                : </small>Maharashtra</label>\n" +
                "                    </td>\n" +
                "                    <td style=\"word-wrap: break-word;\" class=\"border-table\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\" a>State Code :\n" +
                "                                </small>23\n" +
                "                        </label>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "                <tr>\n" +
                "                    <td style=\"word-wrap: break-word;\" class=\"border-table\" colspan=\"3\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\"><b>Customer\n" +
                "                                    Address</b>\n" +
                "                                : </small>Andheri Mumbai</label>\n" +
                "\n" +
                "                    </td>\n" +
                "                    <td style=\"word-wrap: break-word;\" class=\"border-table\" colspan=\"3\">\n" +
                "                        <label> <small style=\"font-weight : bold !Important; word-wrap: break-word\" a>Pin Code :\n" +
                "                                </small>400001\n" +
                "                        </label>\n" +
                "                    </td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <br />\n" +
                "            <table class=\"border-table\">\n" +
                "                <tr>\n" +
                "                    <th class=\"border-table thead\">Product</th>\n" +
                "                    <th class=\"border-table thead\">HSN/SAC</th>\n" +
                "                    <th class=\"border-table thead\">Billed Qty</th>\n" +
                "                    <th class=\"border-table thead\">Rate</th>\n" +
                "                    <th class=\"border-table thead\">total</th>\n" +
                "                </tr>\n" +
                                tableValue()+
                "                <tr>\n" +
                "                    <td class=\"border-table right\" colspan=\"4\"> <b>Total</b></td>\n" +
                "                    <td class=\"border-table right\" colspan=\"4\"> <b>+"+tot+"</b></td>\n" +
                "                </tr>\n" +
                "            </table>\n" +
                "            <br />\n" +
                "            <div>\n" +
                "                <table>\n" +
                "                    <tr>\n" +
                "                        <td>Mode Of Transportation:</td>\n" +
                "                        <td>Road</td>\n" +
                "                        <td class=\"right\">\n" +
                "                            <b style=\"padding-right:60%\"> Total invoice Value: </b>98989<br />\n" +
                "                            <b style=\"padding-right:60%\"> Total invoice Value: </b>98989<br />\n" +
                "                            <b style=\"padding-right:73%\"> (Rounded Off)</b>\n" +
                "                            <hr style=\"height: 0.2rem; background: black;\">\n" +
                "                        </td>\n" +
                "                    </tr>\n" +
                "                </table>\n" +
                "            </div>\n" +
                "            <div style=\"text-align: center;\">\n" +
                "                <h5><b>Total invoice value in word:</b> Three Lakhs Fifty Five Thousand Eight Hundred Forty Rupees</h5>\n" +
                "            </div>\n" +
                "            <div>\n" +
                "                <label>Note :</label><br/>\n" +
                "                <textarea rows=\"4\" cols=\"40\"></textarea>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        \n" +
                "    </div>\n" +
                "</body>\n" +
                "\n" +
                "</html>";
    }

    String tableValue (){
        StringBuffer  stringBuffer=new StringBuffer();

        for (int i=0;i<productAddArrayList.size();i++){
        stringBuffer.append( " <tr>\n" +
                "                    <td class=\"border-table\">"+productAddArrayList.get(i).getProductData().getProductdesc()+"</td>\n" +
                "                    <td class=\"border-table\">100000</td>\n" +
                "                    <td class=\"border-table right\">"+productAddArrayList.get(i).getTotalQuntity()+"</td>\n" +
                "                    <td class=\"border-table right\">"+productAddArrayList.get(i).getRate()+"</td>\n" +
                "                    <td class=\"border-table right\">"+productAddArrayList.get(i).getTotalAount()+"</td>\n" +
                "                </tr>\n" );


        }


        return  stringBuffer.toString();
    }
}