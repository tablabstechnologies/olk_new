package com.pk.olk

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.pk.olk.adapter.CustomerAdapter
import com.pk.olk.databinding.ActivityCustomerListBinding
import com.pk.olk.model.CustmerData
import com.pk.olk.model.CustomerList
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CustomerListActivity : AppCompatActivity() {
    lateinit var binding:ActivityCustomerListBinding
    lateinit var listCustomer:ArrayList<CustmerData>
    lateinit var adpter:CustomerAdapter
     var data: String? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_customer_list)
        listCustomer= ArrayList()
        data= intent.getStringExtra("data")
        if(data.equals("1")){
            callgetCustomer()
            binding.appBar.setText("Customer")

        }else{
            callGetSupplier()
            binding.appBar.setText("Supplier")
        }

        binding.listCustomer.layoutManager=LinearLayoutManager(this)
        adpter= CustomerAdapter(this,listCustomer)
        binding.listCustomer.adapter=adpter

        binding.imgAddCust.setOnClickListener {

            startActivity(Intent(this,AddCustomerActivity::class.java))
        }


    }

    private fun callGetSupplier() {
        try {
            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getSupplier(token)
            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()
            call.enqueue(object : Callback<CustomerList> {
                override fun onResponse(
                    call: Call<CustomerList>,
                    response: Response<CustomerList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {
                        listCustomer.addAll(response.body()!!.gkresult)
                        adpter.notifyDataSetChanged()

                        if (listCustomer.size==0){
                            binding.errorMsg.visibility=View.VISIBLE
                        }

                    } else {

                    }

                }

                override fun onFailure(call: Call<CustomerList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@CustomerListActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }

    }

    private fun callgetCustomer() {

        try {
            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getCustomer(token)
            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()
            call.enqueue(object : Callback<CustomerList> {
                override fun onResponse(
                    call: Call<CustomerList>,
                    response: Response<CustomerList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {
                        listCustomer.addAll(response.body()!!.gkresult)
                        adpter.notifyDataSetChanged()
                        if (listCustomer.size==0){
                            binding.errorMsg.visibility=View.VISIBLE
                        }

                    } else {

                    }

                }

                override fun onFailure(call: Call<CustomerList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@CustomerListActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }
}