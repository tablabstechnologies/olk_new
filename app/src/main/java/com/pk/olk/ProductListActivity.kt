package com.pk.olk

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.pk.olk.adapter.ProductAdapter
import com.pk.olk.databinding.ActivityProductListBinding
import com.pk.olk.model.ProductData
import com.pk.olk.model.ProductList
import com.pk.olk.retrofit.SaasCall
import com.pk.olk.utils.CustomeToast
import com.pk.olk.utils.Utils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductListActivity : AppCompatActivity() {
    lateinit var binding:ActivityProductListBinding
    lateinit var adpter:ProductAdapter
    lateinit var listProduct:ArrayList<ProductData>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_product_list)
        binding.productList.layoutManager=LinearLayoutManager(this)
        listProduct= ArrayList()
        adpter= ProductAdapter(this,listProduct)
        binding.productList.adapter=adpter
        binding.imgAddProduct.setOnClickListener {

            startActivity(Intent(this,AddProductActivity::class.java))
        }
        callApi()

    }


    private fun callApi() {
        try {

            val token = Utils.getCompanyData(this)!!
            val call = SaasCall.apiInterfacePaas.getProducts(token)

            val progressDailog: ProgressDialog = ProgressDialog(this)
            progressDailog.setCancelable(false)
            progressDailog.setMessage("Loading....")
            progressDailog.show()


            call.enqueue(object : Callback<ProductList> {
                override fun onResponse(
                    call: Call<ProductList>,
                    response: Response<ProductList>
                ) {
                    progressDailog.dismiss()

                    if (response.body()?.gkstatus == 0) {

                        listProduct.addAll(response.body()!!.gkresult)
                        adpter.notifyDataSetChanged()



                    } else {

                    }

                }

                override fun onFailure(call: Call<ProductList>, t: Throwable) {
                    //TODO("Not yet implemented")
                    CustomeToast.Show(this@ProductListActivity, "User Already Found")
                    println("The Response is Fail;")
                    println("The Response is Fail")
                    progressDailog.dismiss()

                }

            })
        } catch (e: Exception) {

            e.printStackTrace()
        }
    }
}