package com.pk.olk.retrofit

import CompanyData
import UnitList
import com.google.gson.JsonObject
import com.pk.olk.model.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiServices {

    @GET
    fun checkUserIsValid(@Url url:String ) : Call<GenericModelClass>

    @POST("/login")
    fun login(@Body body: RequestBody) : Call<GenericModelClass>

    @POST("/accounts?forgotpassword")
    fun forgotpassword(@Body body: RequestBody) : Call<GenericModelClass>

    @POST("/accounts?email")
    fun sendVeriicationCode(@Body body: RequestBody) : Call<GenericModelClass>

    @POST("/accounts?matchcode")
    fun matchcode(@Body body: RequestBody) : Call<GenericModelClass>


    @POST("/accounts?register")
    fun register(@Body body: RequestBody) : Call<GenericModelClass>

    @GET("/accounts?profile")
    fun accountProile(@Header("saastoken")token:String) : Call<CompanyData>

    @POST("/accounts?createcompany")
    fun createcompany(@Header("saastoken")token:String,@Body body: RequestBody) : Call<GenericModelClass>


    @POST("/accounts?gklogin")
    fun gkLogin(@Header("saastoken")token:String,@Body body: RequestBody) : Call<GenericModelClass>


    @POST("/customersupplier")
    fun createCustomerSupplier(@Header("gktoken")token:String,@Body body: RequestBody) : Call<GenericModelClass>


    @GET("/unitofmeasurement?qty=all")
    fun getAllUnit(@Header("gktoken")token:String) : Call<UnitList>


    @POST("/products")
    fun createproducts(@Header("gktoken")token:String,@Body body: RequestBody) : Call<GenericModelClass>


    @GET("/products")
    fun getProducts(@Header("gktoken")token:String) : Call<ProductList>

    @GET("/tax?pscflag=i")
    fun getTax(@Header("gktoken")token:String,@Query("productcode")productcode:Int) : Call<JsonObject>

    @GET("/customersupplier?qty=custall")
    fun getCustomer(@Header("gktoken")token:String) : Call<CustomerList>

    @GET("/customersupplier?qty=supall")
    fun getSupplier(@Header("gktoken")token:String) : Call<CustomerList>

    @GET("/customersupplier?qty=single")
    fun getSupplierDeatils(@Header("gktoken")token:String,@Query("custid")productcode:Int) : Call<CustomerDetails>



}