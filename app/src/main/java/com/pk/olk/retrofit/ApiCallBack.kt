package com.pk.olk.retrofit

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.pk.olk.model.GenericModelClass
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object ApiCallBack {

    var serviceSetterGetter = MutableLiveData<GenericModelClass>()

    fun checkUserValid(password:String,email:String): MutableLiveData<GenericModelClass> {

        val objects:JsonObject= JsonObject()
        objects.addProperty("accountemail",email)
        objects.addProperty("accountpassword",password)
        objects.addProperty("accounttype","3")

        val body: RequestBody = RequestBody.create(
            "application/json; charset=utf-8".toMediaTypeOrNull(),
            objects.toString()
        )


        val call = SaasCall.apiInterface.login(body)


        call.enqueue(object: Callback<GenericModelClass> {
            override fun onFailure(call: Call<GenericModelClass>, t: Throwable) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", t.message.toString())
                println("In Sucess fail")

                println("In Sucess fail")
            }

            override fun onResponse(
                call: Call<GenericModelClass>,
                response: Response<GenericModelClass>
            ) {
                // TODO("Not yet implemented")
                Log.v("DEBUG : ", response.body().toString())

                val data = response.body()
                serviceSetterGetter.value=response.body()

                println("In Sucess"+response.code())

                println("In Sucess"+response.body())


            }
        })
        Log.e("DiString",call.request().url.toString()+call.request().body.toString());

        return serviceSetterGetter

    }


}